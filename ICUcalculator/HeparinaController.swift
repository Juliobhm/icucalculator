//
//  HeparinaController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 14/10/18.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class HeparinaView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate{
    
    
    @IBOutlet var valoresView: UIView!
    @IBOutlet weak var calculosView: UIView!
    
   // @IBOutlet weak var pickerView: UIPickerView!
    //var pickerView = UIPickerView()
    
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var barraPicker: UIToolbar!
    
    @IBOutlet var pesoTextField: UITextField!
    
    //@IBOutlet weak var pesoTextField: UITextField!
    @IBOutlet weak var tallaTextField: UITextField!
    @IBOutlet weak var sexoTextField: UITextField!
    @IBOutlet weak var apttTextField: UITextField!
    @IBOutlet weak var velocidadBombaTextField: UITextField!
    @IBOutlet weak var pesoIdealLabel: UILabel!
    @IBOutlet weak var limiteObesidadLabel: UILabel!
    @IBOutlet weak var pesoAjustadoLabel: UILabel!
    
    @IBOutlet weak var accionesView: UIView!
    @IBOutlet var proximoApttView: UITextView!
    @IBOutlet var accionesTextView: UITextView!
    @IBOutlet var accionesLabel: UILabel!
    @IBOutlet var calculosFondoImageView: UIImageView!
    var pesoCalculosLabel: String!
    var primerValor: String = ""
    var segundoValor: String = ""
    var tercerValor: String = ""
    var marcadorCampo: String = ""
    var pesoIdeal: Double!
    var limiteObesidad: Double!
    var pesoAjustado: Double!
    var pesoCalculos: Double!
    var peso: Double!
    var bolo: String = ""
    var incremento: String = ""
    var pesoV: Double!
    var tallaV: Double!
    var apttV: Double!
    var velocidadV: Double!
    var pesoS: String!
    var tallaS: String!
    var apttS: String!
    var velocidadS: String!
    var nuevaVelocidad: Double!
    var valor: String = ""
    //var pesoRecEntero: String = ""
    var indicePesoEntero: Int = 0
    var indiceTallaEntero: Int = 0
    var indiceApttEntero: Int = 0
    var indiceVelocidadEntero: Int = 0
    var indicePesoDecimal: Int = 0
    var indiceTallaDecimal: Int = 0
    var indiceApttDecimal: Int = 0
    var indiceVelocidadDecimal: Int = 0
    var indiceSexo: Int = 0
    
    var pesoDefecto: String!
    //var indicePesoEnteroDefecto: Int = 0
    //var indicePesoDecimalDefecto: Int = 0
    var pesoVDefault: Double = 70.0
    var tallaDefecto: String!
   // var indiceTallaEnteroDefecto: Int = 0
   // var indiceTallaDecimalDefecto: Int = 0
    var tallaVDefault: Double = 170.0
    var apttDefecto: String!
    //var indiceApttEnteroDefecto: Int = 0
   // var indiceApttDecimalDefecto: Int = 0
    var velocidadDefecto: String!
    //var indiceVelocidadEnteroDefecto: Int = 0
   // var indiceVelocidadDecimalDefecto: Int = 0
    var sexoDefecto: String!
   
    var indiceSexoDefecto: Int = 0
    
    
    let entero = ["","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","115","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","134","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205"]

    let enteroPeso = ["","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","115","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","134","136","137","138","139","140"]
    let enteroTalla = ["","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","115","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","134","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205"]
    let enteroAptt = ["","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","115","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","134","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180"]
    let enteroVelocidad = ["","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50"]
    
    let decimal = ["", "0", "1","2","3","4", "5", "6", "7", "8" ,"9"]
    let sexo = ["", "Hombre", "Mujer"]
    
    
    @IBAction func infoHeparinaButton(_ sender: Any) {
        performSegue(withIdentifier: "infoHeparina", sender: self)
    }
    
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoHeparina"{
            guard let transfVC = segue.destination as? AyudaHeparina else {return}
            transfVC.pesoValueLabel = pesoTextField.text
            transfVC.pesoIdealValueLabel = pesoIdealLabel.text
            transfVC.limiteObesidadValueLabel = limiteObesidadLabel.text
            transfVC.pesoAjustadoValueLabel = pesoAjustadoLabel.text
            transfVC.pesoCalculos = pesoCalculos
            transfVC.velocidadBomba = velocidadV
        }
        
    }
 func textFieldDidBeginEditing(_ textField: UITextField) {
    
    pickerView.isHidden = false
    barraPicker.isHidden = false
    accionesView.isHidden = true
    accionesLabel.isHidden = true

    for view in valoresView.subviews {
        if (view is UITextField) {
            let textField = view as! UITextField
            textField.backgroundColor = .white}
    }
    
    textField.backgroundColor = UIColor(red: 210/255, green: 232/255, blue: 208/255, alpha: 1.0)
    textField.endEditing(true)
    
    
        
        switch textField {
            case pesoTextField:
                marcadorCampo = "Peso"
                print(marcadorCampo)
               if indicePesoEntero == 0{
                    pickerView.selectRow(36, inComponent: 0, animated: false)
                                }
                
               else if indicePesoEntero != 0{
                    pickerView.selectRow(indicePesoEntero, inComponent: 0, animated: true)
                    print(indicePesoEntero)}
                if indicePesoDecimal == 0{
                    pickerView.selectRow(0, inComponent: 1, animated: false)   }
                else if indicePesoDecimal != 0{
                    pickerView.selectRow(indicePesoDecimal, inComponent: 1, animated: false)}
        
        
        case tallaTextField:
                marcadorCampo = "Talla"
                print(marcadorCampo)
                if indiceTallaEntero == 0{
                    pickerView.selectRow(71, inComponent: 0, animated: false) }
                else if indiceTallaEntero != 0{
                    pickerView.selectRow(indiceTallaEntero, inComponent: 0, animated: true)}

                if indiceTallaDecimal == 0{
                    pickerView.selectRow(0, inComponent: 1, animated: false)   }
                else if indicePesoDecimal != 0{
                    pickerView.selectRow(indiceTallaDecimal, inComponent: 1, animated: false)}
            
        case sexoTextField:
                marcadorCampo = "Sexo"
                if indiceSexo == 0{
                    pickerView.selectRow(0, inComponent: 2, animated: false)}
     
 
        case apttTextField:
                marcadorCampo = "APTT"
                print(marcadorCampo)
                if indiceApttEntero == 0{
                    pickerView.selectRow(21, inComponent: 0, animated: false) }
                
                else if indiceApttEntero != 0{
                    pickerView.selectRow(indiceApttEntero, inComponent: 0, animated: true)}
                
                if indicePesoDecimal == 0{
                    pickerView.selectRow(0, inComponent: 1, animated: false)   }
                else if indicePesoDecimal != 0{
                    pickerView.selectRow(indiceApttDecimal, inComponent: 1, animated: false)}
           
        case velocidadBombaTextField:
                marcadorCampo = "Velocidad"
                print(marcadorCampo)
                if indiceVelocidadEntero == 0{
                    pickerView.selectRow(16, inComponent: 0, animated: false)}
                else if indiceVelocidadEntero != 0{
                    pickerView.selectRow(indiceVelocidadEntero, inComponent: 0, animated: true)}
                if indicePesoDecimal == 0{
                    pickerView.selectRow(0, inComponent: 1, animated: false)   }
                else if indicePesoDecimal != 0{
                    pickerView.selectRow(indiceVelocidadDecimal, inComponent: 1, animated: true)}
           
        default:
                marcadorCampo = ""}
    

    
    }
    
    func crearBarraPicker(){
        barraPicker.barStyle = .blackOpaque
        barraPicker.tintColor = .white
        barraPicker.backgroundColor = UIColor(red: 0.2549, green: 0.2549, blue: 0.949, alpha: 1.0)
}
    
    func crearPickerView(){
        barraPicker.barStyle = .blackTranslucent
        barraPicker.tintColor = .white
        pickerView.layer.borderWidth = 2
        pickerView.layer.borderColor = UIColor(red: 0.2549, green: 0.2549, blue: 0.949, alpha: 1.0).cgColor
    }
    
    @IBAction func hechoButton(_ sender: Any) {
        barraPicker.isHidden = true
        pickerView.isHidden = true
        accionesLabel.isHidden = false
        for view in self.view.subviews {
            if (view is UITextField) {
                let textField = view as! UITextField
                textField.backgroundColor = .white}
            }
    }
    
    @IBAction func resetHeparinaButton(_ sender: Any) {
        accionesView.isHidden = true
        barraPicker.isHidden = true
        pickerView.isHidden = true
        accionesLabel.isHidden = false
        for view in self.valoresView.subviews {
            if (view is UITextField) {
                let textField = view as! UITextField
                textField.backgroundColor = .white
                textField.text = nil
            }
        }
        // Borrar valores por defecto
        UserDefaults.standard.set("", forKey: "pesoEnteroDefault")
        UserDefaults.standard.set("", forKey: "pesoEnteroDefault")
        UserDefaults.standard.set("", forKey: "tallaEnteroDefault")
        UserDefaults.standard.set("", forKey: "tallaDecimalDefault")
        UserDefaults.standard.set("", forKey: "apttEnteroDefault")
        UserDefaults.standard.set("", forKey: "apttDecimalDefault")
        UserDefaults.standard.set("", forKey: "velocidadEnteroDefault")
        UserDefaults.standard.set("", forKey: "velocidadDecimalDefault")
        UserDefaults.standard.set("", forKey: "sexoEnteroDefault")
        indicePesoEntero = 0
        indicePesoDecimal = 0
        indiceTallaEntero = 0
        indicePesoDecimal = 0
        indiceApttEntero = 0
        indiceApttDecimal = 0
        indiceVelocidadEntero = 0
        indiceVelocidadDecimal = 0
        indiceSexo = 0
        pesoV = nil
        asignarValores()
        
    }
    
    @IBAction func resultadoButton(_ sender: Any) {
        accionesView.isHidden = false
         barraPicker.isHidden = true
         pickerView.isHidden = true
         for view in self.valoresView.subviews {
             if (view is UITextField) {
             let textField = view as! UITextField
             textField.backgroundColor = .white}
            }
        
        
        
        
}
    override func viewDidLoad() {
        super.viewDidLoad()
        accionesView.layer.borderWidth = 2
        accionesView.layer.borderColor = UIColor(red: 0.2549, green: 0.2549, blue: 0.949, alpha: 1.0).cgColor
        accionesLabel.layer.borderWidth = 2
        accionesLabel.layer.borderColor = UIColor(red: 0.2549, green: 0.2549, blue: 0.949, alpha: 1.0).cgColor
        calculosFondoImageView.layer.borderWidth = 1
        calculosFondoImageView.layer.borderColor = UIColor(red: 0.2549, green: 0.2549, blue: 0.949, alpha: 1.0).cgColor

      
        
        //Añadir valores por defecto a los diferentes campos
 
       // pesoTextField.text = UserDefaults.standard.string(forKey: "pesoDefault")
        indicePesoEntero = UserDefaults.standard.integer(forKey: "pesoEnteroDefault")
        indicePesoDecimal = UserDefaults.standard.integer(forKey: "pesoDecimalDefault")
        
       //tallaTextField.text = UserDefaults.standard.string(forKey: "tallaDefault")
        indiceTallaEntero = UserDefaults.standard.integer(forKey: "tallaEnteroDefault")
        indiceTallaDecimal = UserDefaults.standard.integer(forKey: "tallaDecimalDefault")
    
       // apttTextField.text = UserDefaults.standard.string(forKey: "apttDefault")
        indiceApttEntero = UserDefaults.standard.integer(forKey: "apttEnteroDefault")
        indiceApttDecimal = UserDefaults.standard.integer(forKey: "apttDecimalDefault")
        
        //velocidadBombaTextField.text = UserDefaults.standard.string(forKey: "velocidadDefault")
        indiceVelocidadEntero = UserDefaults.standard.integer(forKey: "velocidadEnteroDefault")
        indiceVelocidadDecimal = UserDefaults.standard.integer(forKey: "velocidadDecimalDefault")
        
        //sexoTextField.text = UserDefaults.standard.string(forKey: "sexoDefault")
        indiceSexo = UserDefaults.standard.integer(forKey: "sexoEnteroDefault")
        //pesoV = UserDefaults.standard.double(forKey: "pesoVDefault")
        //tallaV = UserDefaults.standard.double(forKey: "tallaVDefault")
       
        crearBarraPicker()
        crearPickerView()
        asignarValores()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }

  
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            switch marcadorCampo{
            case "Peso":
                return enteroPeso.count
            case "Talla":
                return enteroTalla.count
            case "APTT":
                return enteroAptt.count
            case "Velocidad":
                return enteroVelocidad.count
            default:
                return entero.count}
        }
            else if component == 1{
                return decimal.count }
        
            return sexo.count
        }
    
func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // pickerView.selectRow(100, inComponent: 0, animated: true)
 
       
        
        
        if component == 0{
          switch marcadorCampo{
                case "Peso":
                    return enteroPeso[row]
                case "Talla":
                    return enteroTalla[row]
                case "APTT":
                    return enteroAptt[row]
                case "Velocidad":
                    return enteroVelocidad[row]
                default:
                    return entero[row]}
 
        }
        else if component == 1
             {return decimal[row]}
        return sexo[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        switch marcadorCampo{
                case "Peso":
                    //primerValor = enteroPeso[pickerView.selectedRow(inComponent: 0)]
                    indicePesoEntero = pickerView.selectedRow(inComponent: 0)
          
                    UserDefaults.standard.set(indicePesoEntero, forKey: "pesoEnteroDefault")
                case "Talla":
                    //primerValor = enteroTalla[pickerView.selectedRow(inComponent: 0)]
                    indiceTallaEntero = pickerView.selectedRow(inComponent: 0)
                
                    UserDefaults.standard.set(indiceTallaEntero, forKey: "tallaEnteroDefault")
            
            
                case "APTT":
                    //primerValor = enteroAptt[pickerView.selectedRow(inComponent: 0)]
                    indiceApttEntero = pickerView.selectedRow(inComponent: 0)
                    
                    UserDefaults.standard.set(indiceApttEntero, forKey: "apttEnteroDefault")
                case "Velocidad":
                    //primerValor = enteroVelocidad[pickerView.selectedRow(inComponent: 0)]
                    indiceVelocidadEntero = pickerView.selectedRow(inComponent: 0)
                  
                    UserDefaults.standard.set(indiceVelocidadEntero, forKey: "velocidadEnteroDefault")
                default:
                    break}

  
     
        switch marcadorCampo{
            case "Peso":
                //segundoValor = decimal[pickerView.selectedRow(inComponent: 1)]
                indicePesoDecimal = pickerView.selectedRow(inComponent: 1)

                UserDefaults.standard.set(indicePesoDecimal, forKey: "pesoDecimalDefault")
 
            case "Talla":
                //segundoValor = decimal[pickerView.selectedRow(inComponent: 1)]
                indiceTallaDecimal = pickerView.selectedRow(inComponent: 1)

                UserDefaults.standard.set(indiceTallaDecimal, forKey: "tallaDecimalDefault")
            case "APTT":
                //segundoValor = decimal[pickerView.selectedRow(inComponent: 1)]
                indiceApttDecimal = pickerView.selectedRow(inComponent: 1)

                UserDefaults.standard.set(indiceApttDecimal, forKey: "apttDecimalDefault")
            case "Velocidad":
                //segundoValor = decimal[pickerView.selectedRow(inComponent: 1)]
                indiceVelocidadDecimal = pickerView.selectedRow(inComponent: 1)

                UserDefaults.standard.set(indiceVelocidadDecimal, forKey: "velocidadDecimalDefault")
            default:
                break}
        
        //tercerValor = sexo[pickerView.selectedRow(inComponent: 2)]
        indiceSexo = pickerView.selectedRow(inComponent: 2)
        //indiceSexoDefecto = indiceSexo
        UserDefaults.standard.set(indiceSexo, forKey: "sexoEnteroDefault")
       
        asignarValores()

        }
        
        func asignarValores(){
            if indicePesoEntero > 0 && indicePesoDecimal > 0  {
                pesoS = "\(enteroPeso[indicePesoEntero]).\(decimal[indicePesoDecimal]) kg"}
            else if indicePesoEntero > 0 && indicePesoDecimal == 0 {
                pesoS = "\(enteroPeso[indicePesoEntero]) kg"}
            else if indicePesoEntero == 0 && indicePesoDecimal  > 0{
                pesoS = "0.\(decimal[indicePesoDecimal]) kg"}
            else if indicePesoEntero == 0 && indicePesoDecimal == 0{
                pesoS = ""}
            pesoTextField.text = pesoS
            let pesoX = "\(enteroPeso[indicePesoEntero]).\(decimal[indicePesoDecimal])"
            pesoV = Double(pesoX)
   
            if indiceTallaEntero > 0 && indiceTallaDecimal >  0{
                tallaS = "\(enteroTalla[indiceTallaEntero]).\(decimal[indiceTallaDecimal]) cm"}
            else if indiceTallaEntero > 0 && indiceTallaDecimal == 0 {
                tallaS = "\(enteroTalla[indiceTallaEntero]) cm"}
            else if indiceTallaEntero == 0 && indiceTallaDecimal  > 0{
                tallaS = "0.\(decimal[indiceTallaDecimal]) cm"}
            else if indiceTallaEntero == 0 && indiceTallaDecimal == 0{
                tallaS = ""}
            tallaTextField.text = tallaS
            let tallaX = "\(enteroTalla[indiceTallaEntero]).\(decimal[indiceTallaDecimal])"
            tallaV = Double(tallaX)
            
            if indiceApttEntero > 0 && indiceApttDecimal >  0{
                apttS = "\(enteroAptt[indiceApttEntero]).\(decimal[indiceApttDecimal]) s"}
            else if indiceApttEntero > 0 && indiceApttDecimal == 0 {
                apttS = "\(enteroAptt[indiceApttEntero]) s"}
            else if indiceApttEntero == 0 && indiceApttDecimal  > 0{
                apttS = "0.\(decimal[indiceApttDecimal]) s"}
            else if indiceApttEntero == 0 && indiceApttDecimal == 0{
                apttS = ""}
            apttTextField.text = apttS
            let apttX = "\(enteroAptt[indiceApttEntero]).\(decimal[indiceApttDecimal])"
            apttV = Double(apttX)
            
            if indiceVelocidadEntero > 0 && indiceVelocidadDecimal >  0{
                velocidadS = "\(enteroVelocidad[indiceVelocidadEntero]).\(decimal[indiceVelocidadDecimal]) mL/h"}
            else if indiceVelocidadEntero > 0 && indiceVelocidadDecimal == 0 {
                velocidadS = "\(enteroVelocidad[indiceVelocidadEntero]) mL/h"}
            else if indiceVelocidadEntero == 0 && indiceVelocidadDecimal  > 0{
                velocidadS = "0.\(decimal[indiceVelocidadDecimal]) mL/h"}
            else if indiceVelocidadEntero == 0 && indiceVelocidadDecimal == 0{
                velocidadS = ""}
            velocidadBombaTextField.text = velocidadS
            let velocidadX = "\(enteroVelocidad[indiceVelocidadEntero]).\(decimal[indiceVelocidadDecimal])"
            velocidadV = Double(velocidadX)
            
            sexoTextField.text = sexo[indiceSexo]
    
            
            rellenarCampos()
    }
        
        

    func rellenarCampos(){
        if pesoV == nil {
            pesoCalculos = -1
            pesoIdealLabel.text = "--"
            limiteObesidadLabel.text = "--"
            pesoAjustadoLabel.text = "--"
        }
        else if pesoTextField.text != ""{
            if tallaTextField.text == "" || sexoTextField.text == ""{
                pesoCalculos = pesoV
                pesoIdealLabel.text = "--"
                limiteObesidadLabel.text = "--"
                pesoAjustadoLabel.text = "--"
            }
            else if tallaTextField.text != "" && sexoTextField.text != ""{

                if sexoTextField.text == "Hombre"{
                    peso = pesoV
                    pesoIdeal = ((tallaV / 2.54) - 60) * 2.3 + 50 }
                else if sexoTextField.text == "Mujer"{
                    peso = pesoV
                    pesoIdeal = ((tallaV / 2.54) - 60) * 2.3 + 45.5 }
              
                limiteObesidad = pesoIdeal * 1.3
                pesoAjustado = pesoIdeal + ((peso - pesoIdeal) * 0.25)
                pesoIdealLabel.text = String(format: "%.1f", pesoIdeal) + " kg"
                limiteObesidadLabel.text = String(format: "%.1f",limiteObesidad) + " kg"
                pesoAjustadoLabel.text = String(format: "%.1f",pesoAjustado) + " kg"
                if peso > limiteObesidad {
                    pesoCalculos = pesoAjustado}
             
                else if peso <= limiteObesidad{
                    pesoCalculos = peso}
             
            
            }
        }
        
        UserDefaults.standard.set(pesoCalculos, forKey: "pesoCalculosDefault")
        
        
        if apttV == nil{
                accionesTextView.text = ""
                proximoApttView.text = ""
        }
            else if apttV != nil {
               
                let apttX = Double(apttV)
                if pesoCalculos == -1 {
                    switch apttX {
                        case ..<35:
                            accionesTextView.text = "No se puede calcular el bolo necesario sin el peso\n" + "Incrementar la dosis en 4 ui/kg/h"
                            proximoApttView.text = "En 6 horas"
                        case 35..<46:
                            accionesTextView.text = "No se puede calcular el bolo necesario sin el peso\n" + "Incrementar la dosis en 2 ui/kg/h"
                            proximoApttView.text = "En 6 horas"
                        case 46..<75:
                            accionesTextView.text = "Sin cambios, rango terapéutico"
                            proximoApttView.text = "En 6 horas\n" + "Cuando se consigan 2 APTTs en rango terapéutico, el siguiente APTT por la mañana"
                        case 75..<90:
                            accionesTextView.text = "Descender la dosis en 2 ui/kg/h"
                            proximoApttView.text = "En 6 horas"
                        case 90...:
                            accionesTextView.text = "Parar la perfusion durante una hora y después:\n)" + "   Descender la dosis en 3 ui/kg/h"
                            proximoApttView.text = "En 6 horas"
                        default:
                            break }
                }
                else if pesoCalculos != -1{
                    switch apttX {
                        case ..<35:
                            bolo = String(format: "%.0f",((pesoCalculos)! * 80))
                            incremento = String(format: "%.1f",(4 * 270 * pesoCalculos / 20000))
                            accionesTextView.text = "Administrar un bolo de heparina de \(bolo) unidades\n" + "Incrementar la dosis en 4 ui/kg/h\n" + "Incrementar la perfusión en \(incremento) mL/h"
                            if velocidadV != nil{
                                nuevaVelocidad = velocidadV! + (Double(incremento))!
                                if let nuevaVelocidadDesenmascarada = nuevaVelocidad{
                                    accionesTextView.text = accionesTextView.text + "\n Nueva velocidad: \(nuevaVelocidadDesenmascarada) mL/h"}
                            }
                            proximoApttView.text = "En 6 horas"
                        case 35..<46:
                            bolo = String(format: "%.0f",(pesoCalculos * 40))
                            incremento = String(format: "%.1f",(2 * 270 * pesoCalculos / 20000))
                            accionesTextView.text = "Administrar un bolo de heparina de \(bolo) unidades\n" + "Incrementar la dosis en 2 ui/kg/h\n" + "Incrementar la perfusión en \(incremento) mL/h"
                            if velocidadV != nil{
                                nuevaVelocidad = velocidadV! + (Double(incremento))!
                                if let nuevaVelocidadDesenmascarada = nuevaVelocidad{
                                     accionesTextView.text = accionesTextView.text + "\n Nueva velocidad: \(nuevaVelocidadDesenmascarada) mL/h"}
                            }
                            proximoApttView.text = "En 6 horas"
                        case 46...75:
                            accionesTextView.text = "Sin cambios, rango terapéutico"
                            proximoApttView.text = "En 6 horas\n" + "Cuando se consigan 2 APTTs en rango terapéutico, el siguiente APTT por la mañana"
                        case 75...90:
                            incremento = String(format: "%.1f",(2 * 270 * pesoCalculos / 20000))
                            accionesTextView.text = "Descender la dosis en 2 ui/kg/h\n" + "Descender la perfusión en \(incremento) mL/h"
                            if velocidadV != nil{
                                nuevaVelocidad = velocidadV! - (Double(incremento))!
                                if let nuevaVelocidadDesenmascarada = nuevaVelocidad{
                                   accionesTextView.text = accionesTextView.text + "\n Nueva velocidad: \(nuevaVelocidadDesenmascarada) mL/h"}
                            }
                            proximoApttView.text = "En 6 horas"
                        case 90...:
                            incremento = String(format: "%.1f",(3 * 270 * pesoCalculos / 20000))
                            accionesTextView.text = "Parar la perfusion durante una hora y después:\n)" + "   Descender la dosis en 3 ui/kg/h\n" + "   Descender la perfusión en \(incremento) mL/h"
                            if velocidadV != nil{
                                nuevaVelocidad = velocidadV! -
                                    (Double(incremento))!
                                if let nuevaVelocidadDesenmascarada = nuevaVelocidad{
                                     accionesTextView.text = accionesTextView.text + "\n Nueva velocidad: \(nuevaVelocidadDesenmascarada) mL/h"}
                            }
                            proximoApttView.text = "En 6 horas"
                        default:
                            break
                        }
            }

}
}



}
