//
//  ViewController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 9/10/18.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var ScrollView: UIScrollView!
    
    @IBOutlet weak var PrimariaView: UIView!
    
    @IBOutlet var view01: UIView!
    @IBOutlet var view02: UIView!
    
    
    @IBOutlet weak var litrosAireSlider: UISlider!
    @IBOutlet weak var litrosOxigenoSlider: UISlider!
    @IBOutlet weak var litrosAireLabel: UILabel!
    @IBOutlet weak var litrosOxigenoLabel: UILabel!
    @IBOutlet weak var flujoObtenido: UILabel!
    @IBOutlet weak var fio2Obtenido: UILabel!
   
    
    @IBOutlet weak var fio2DeseadoSlider: UISlider!
    @IBOutlet weak var flujoDeseadoSlider: UISlider!
    @IBOutlet weak var fio2DeseadoLabel: UILabel!
    @IBOutlet weak var flujoDeseadoLabel: UILabel!
    @IBOutlet weak var litrosAireNecesarioLabel: UILabel!
    @IBOutlet weak var litrosOxigenoNecesarioLabel: UILabel!
    
    
    @IBAction func cambioLitrosAireSlider(_ sender: UISlider) {
        litrosAireLabel.text =  String(format: "%.0f", litrosAireSlider.value)
        flujoFio2()}
    
    @IBAction func cambioLitrosOxigenoSlider(_ sender: UISlider) {
        litrosOxigenoLabel.text =  String(format: "%.0f", litrosOxigenoSlider.value)
        flujoFio2()}
    
    @IBAction func cambioFio2DeseadoSlider(_ sender: Any) {
        fio2DeseadoLabel.text = String(format: "%.0f", fio2DeseadoSlider.value)
        oxigenoAire()
    }
    
    @IBAction func cambioFluoDeseadoSlider(_ sender: Any) {
        flujoDeseadoLabel.text = String(format: "%.0f", flujoDeseadoSlider.value)
        oxigenoAire()}

    override func viewDidLoad() {
       litrosAireLabel.text = ""
        litrosOxigenoLabel.text = ""
        flujoDeseadoLabel.text = ""
        fio2DeseadoLabel.text = ""
        flujoObtenido.text = ""
        fio2Obtenido.text = ""
        litrosAireNecesarioLabel.text = ""
        litrosOxigenoNecesarioLabel.text = ""
        
        // Inicio método Pinch zoom. Requiere la scrollView delegate"
        self.ScrollView.minimumZoomScale = 1.0
        self.ScrollView.maximumZoomScale = 3
        self.ScrollView.contentSize.height = 1000
        super.viewDidLoad()
        
        
    }
    func viewForZooming(in ScrollView: UIScrollView) -> UIView? {
        return (self.PrimariaView)}
    // Fin método Pinch zoom
    
    // Cálculo Flujo y FiO2
   
    func flujoFio2(){
        flujoObtenido.text = String(format: "%.0f", (litrosAireSlider.value + litrosOxigenoSlider.value))
        if (litrosAireSlider.value + litrosOxigenoSlider.value) <= 0 {
            fio2Obtenido.text = "0"}
        
        let numerador = litrosAireSlider.value * 0.21 + litrosOxigenoSlider.value
        let denominador = litrosAireSlider.value + litrosOxigenoSlider.value
        let fio2 = numerador / denominador
        //let fio2 = (litrosAireSlider.value * 0.21 + litrosOxigenoSlider.value)/(litrosAireSlider.value + litrosOxigenoSlider.value)
        if (litrosAireSlider.value + litrosOxigenoSlider.value) <= 0 {
            fio2Obtenido.text = "0"
            flujoObtenido.text = "0"}
        else {fio2Obtenido.text = String(format: "%.0f", (100 * fio2))
             }
            }
        
    // Cálculo litros de Oxígeno y Aire
    func oxigenoAire(){
        litrosAireNecesarioLabel.text = String(format: "%.0f", (flujoDeseadoSlider.value * ((100 - fio2DeseadoSlider.value)/79)))
        litrosOxigenoNecesarioLabel.text = String(format: "%.0f", (flujoDeseadoSlider.value - (((flujoDeseadoSlider.value * (100 - fio2DeseadoSlider.value)/79)))))
        
        //litrosOxigenoLabel.text = String(format: "%.0f", oxigeno)
        
 
        
    }

}

