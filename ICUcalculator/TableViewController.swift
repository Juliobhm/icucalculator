//
//  TableViewController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 16/10/18.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewController: UITableView!
    let aplicaciones = ["OAF", "Heparina", "Tromboembolismo pulmonar", "Perfusiones continuas", "Nutrición"]
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return aplicaciones.count
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       let cell = UITableViewCell()
        cell.textLabel?.text = aplicaciones[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: aplicaciones[indexPath.row], sender: self)
    }

}
