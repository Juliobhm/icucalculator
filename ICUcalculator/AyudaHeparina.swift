//
//  AyudaHeparina.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 13/11/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class AyudaHeparina: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UINavigationControllerDelegate{
    
 
    @IBOutlet var ayudaHeparinaView: UIView!
    
    @IBOutlet var ayudaHeparinaScrollView: UIScrollView!
    @IBOutlet var pesoLabel: UILabel!
    @IBOutlet var pesoIdealLabel: UILabel!
    @IBOutlet var limiteObesidadLabel: UILabel!
    @IBOutlet var pesoAjustadoLabel: UILabel!
    @IBOutlet var anotacionesTextView: UITextView!
    @IBOutlet var preparacionTextView: UITextView!
    @IBOutlet var heparinizacionLentaTextView: UITextView!
    @IBOutlet var heparinizacionRapidaTextView: UITextView!
    @IBOutlet var protocoloTextView: UITextView!
    var pesoValueLabel: String!
    var pesoIdealValueLabel: String!
    var limiteObesidadValueLabel: String!
    var pesoAjustadoValueLabel: String!
    var pesoCalculos: Double = -1
    var velocidadBomba: Double!
    var acciones = ""
    var heparinizacionLenta = ""
    var heparinizacionRapida = ""
    
    
   
    func viewForZooming(in ScrollView: UIScrollView) -> UIView? {
        return ayudaHeparinaView}
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Procedimiento zooom, requiere class UIScrollViewDelegate y llamada a lafunción viewForZooming
        self.ayudaHeparinaScrollView.minimumZoomScale = 1.0
        self.ayudaHeparinaScrollView.maximumZoomScale = 6.0
        ayudaHeparinaScrollView.contentSize.height = 1000
        pesoLabel.text = pesoValueLabel
        pesoIdealLabel.text = pesoIdealValueLabel
        limiteObesidadLabel.text = limiteObesidadValueLabel
        pesoAjustadoLabel.text = pesoAjustadoValueLabel
    
       
        
        
        
        anotacionesTextView.text = "Si el peso supera el límite de obesidad, se utiliza el peso ajustado\n Peso ideal calculado según la fórmula de Devine\nPreparación de la bomba: 20000 Unidades en 250 mL de Glucosado al 5%. Volumen total 270 mL, 74,1 ui/mL"
        
        //preparacionTextView.text = "Preparación de la bomba: 20000 Unidades en 250 mL de Glucosado al 5%. Volumen total 270 mL, 74,1 ui/mL"
        
        


        
                print("PesoCalculos: \(pesoCalculos)")
        
        if pesoCalculos == -1 {
            pesoLabel.text = "--"
            
            heparinizacionLenta = "Heparinización lenta\nSin bolo\n Inicio de perfusión a 12 unidades/kg/h"
            heparinizacionRapida = "Heparinización rápida\nBolo de 80 ui/kg \nInicio de perfusión a 18 unidades/kg/h"
        
            acciones = "PROTOCOLO DE ACTUACIÓN\n\nAPTT < 35 segundos\nBolo de 80 ui/kg\nIncrementar la dosis en 4 ui/kh/h\nPróximo APTT: En 6 horas\n\nAPTT entre 35 y 45 segundos\nBolo de 40 ui/kg\nIncrementar la dosis en 2 ui/kh/h\nPróximo APTT: En 6 horas\n\nAPTT entre 46 y 70 segundos\nSin cambios. Rango terapéutico\nPróximo APTT: En 6 horas. Cuando se consigan dos APTTs consecutivos en el rango terapéutico, el siguiente APTT por la mañana\n\nAPTT entre 71 y 90 segundos\nDescender la dosis en 2 unidades/Kg/h\nPróximo APTT: En 6 horas\n\nAPTT > 90 segundos\nParar la perfusión durante una hora y después:\nDescender la dosis en 3 ui/kg/h\nPróximo APTT: En 6 horas"
        }
        
       
        else if pesoCalculos > -1{
            
            heparinizacionLenta = "Heparinización lenta\nSin bolo\n Inicio de perfusión a 12 unidades /kg/h\nInicio de la perfusión a \(String(format: "%.1f",((pesoCalculos) * 12 * 270/20000))) mL/h"
   
            heparinizacionRapida = "Heparinización rápida\nBolo de 80 ui/kg (\(String(format: "%.0f",((pesoCalculos) * 80))) unidades)\nInicio de perfusión a 18 unidades/kg/h\nInicio de la perfusión a \(String(format: "%.1f",((pesoCalculos) * 18 * 270/20000))) mL/h"
        
            if velocidadBomba == nil{
                    acciones = "PROTOCOLO DE ACTUACIÓN\n\nAPTT < 35 segundos\nBolo de 80 ui/kg (\(String(format: "%.0f", (pesoCalculos * 80))) unidades)\nIncrementar la dosis en 4 ui/kh/h\nPróximo APTT: En 6 horas\n\nAPTT entre 35 y 45 segundos\nBolo de 40 ui/kg (\(String(format: "%.0f", (pesoCalculos * 40)))unidades)\nIncrementar la dosis en 2 ui/kh/h\n     Próximo APTT: En 6 horas\n\nAPTT entre 46 y 70 segundos\nSin cambios. Rango terapéutico\nPróximo APTT: En 6 horas. Cuando se consigan dos APTTs consecutivos en el rango terapéutico, el siguiente APTT por la mañana\n\nAPTT entre 71 y 90 segundos\nDescender la dosis en 2 unidades/Kg/h\nPróximo APTT: En 6 horas\n\nAPTT > 90 segundos\nParar la perfusión durante una hora y después:\nDescender la dosis en 3 ui/kg/h\nPróximo APTT: En 6 horas"}
        
                else if velocidadBomba != nil{
                    let nuevaVelocidad: String = "Nueva velocidad: (\(String(format: "%.0f", (velocidadBomba + (4 * 270 * pesoCalculos / 20000)))) mL/h\n"
                    acciones = "PROTOCOLO DE ACTUACIÓN\n\nAPTT < 35 segundos\nBolo de 80 ui/kg (\(String(format: "%.0f", (pesoCalculos * 80))) unidades)\nIncrementar la dosis en 4 ui/kh/h\nIncrementar la perfusión en \(String(format: "%.1f",(4 * 270 * pesoCalculos / 20000))) mL/h\nNueva velocidad: \(String(format: "%.0f", (velocidadBomba + (4 * 270 * pesoCalculos / 20000)))) mL/h\nPróximo APTT: En 6 horas\n\nAPTT entre 35 y 45 segundos\nBolo de 40 ui/kg (\(String(format: "%.0f", (pesoCalculos * 40))) unidades)\nIncrementar la dosis en 2 ui/kh/h\n Incrementar la perfusión en \(String(format: "%.1f",(2 * 270 * pesoCalculos / 20000))) mL/h\nNueva velocidad: \(String(format: "%.0f", (velocidadBomba + (2 * 270 * pesoCalculos / 20000)))) mL/h\nPróximo APTT: En 6 horas\n\nAPTT entre 46 y 70 segundos\nSin cambios. Rango terapéutico\nPróximo APTT: En 6 horas. Cuando se consigan dos APTTs consecutivos en el rango terapéutico, el siguiente APTT por la mañana\n\nAPTT entre 71 y 90 segundos\nDescender la dosis en 2 unidades/Kg/h\nDescender la perfusión en \(String(format: "%.1f",(2 * 270 * pesoCalculos / 20000))) mL/h\nNueva velocidad: \(String(format: "%.0f", (velocidadBomba - (2 * 270 * pesoCalculos / 20000)))) mL/h\nPróximo APTT: En 6 horas\n\nAPTT > 90 segundos\nParar la perfusión durante una hora y después:\nDescender la dosis en 3 ui/kg/h\nDescender la perfusión en \(String(format: "%.1f",(3 * 270 * pesoCalculos / 20000))) mL/h\nNueva velocidad: \(String(format: "%.0f", (velocidadBomba - (3 * 270 * pesoCalculos / 20000)))) mL/h\nPróximo APTT: En 6 horas"                                                 }
           
    }
          
        
           var heparinizacionLentaModificada: NSMutableAttributedString = NSMutableAttributedString(string: heparinizacionLenta)
            heparinizacionLentaModificada = NSMutableAttributedString(string: heparinizacionLenta)
            var  cambioColor = NSMutableAttributedString(string: heparinizacionLenta)
            var posicion = (cambioColor.string as NSString).range(of: "Heparinización lenta")
            heparinizacionLentaModificada.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0, green: 0.2157, blue: 0.7294, alpha: 1.0),range: posicion)
            heparinizacionLentaTextView.attributedText = heparinizacionLentaModificada
        
            var heparinizacionRapidaModificada: NSMutableAttributedString = NSMutableAttributedString(string: heparinizacionRapida)
            heparinizacionRapidaModificada = NSMutableAttributedString(string: heparinizacionRapida)
             cambioColor = NSMutableAttributedString(string: heparinizacionRapida)
             posicion = (cambioColor.string as NSString).range(of: "Heparinización rápida")
            heparinizacionRapidaModificada.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0, green: 0.2157, blue: 0.7294, alpha: 1.0),range: posicion)
            heparinizacionRapidaTextView.attributedText = heparinizacionRapidaModificada
            
        
            let expresion = ["PROTOCOLO DE ACTUACIÓN", "Próximo APTT:", "APTT < 35 segundos", "APTT entre 35 y 45 segundos", "APTT entre 46 y 70 segundos","APTT entre 71 y 90 segundos", "APTT > 90 segundos"]
            var accionesModificada: NSMutableAttributedString = NSMutableAttributedString(string: acciones)
            var index = 0
            for valores in expresion{
                
                for char in acciones {
                  
                    
                    // Se aislan todas las palabras y se les asigna un rango de tipo index (rangoIndex)
                    
                    if (acciones.count - index) >= 0 {    // Previene que el inicio no sobrepase la lontitud de la palabra
                        let inicioExpresion = acciones.index(acciones.startIndex, offsetBy: index) // Obtiene un valor tipo index para el inicio del rango
                        let inicioRango = acciones.distance(from: acciones.startIndex, to: inicioExpresion) // Transforma el valor index en Int
                        let letraFinal = acciones.index(acciones.endIndex, offsetBy: 0)
                        let ultimaPosicion = acciones.distance(from: acciones.startIndex, to: letraFinal)
                        if index <= (ultimaPosicion - valores.count){ // Previene que no se evalúen letras que sobrepasen la longitud de la expresión.
                            let finExpresion = acciones.index(acciones.startIndex, offsetBy: (valores.count + index))
                            let finRango = acciones.distance(from: acciones.startIndex, to: finExpresion)
                            let rangoIndex = inicioExpresion..<finExpresion
                            let longitudRango = finRango - inicioRango
                            let nsRango = NSMakeRange(inicioRango, longitudRango)
                            if acciones.substring(with: rangoIndex) == valores {
                                accionesModificada.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0, green: 0.2157, blue: 0.7294, alpha: 1.0), range: nsRango)
                                if valores == "Próximo APTT:"
                                {accionesModificada.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.0353, green: 0.4667, blue: 0, alpha: 1.0), range: nsRango)
                                }
                                }
                            index += 1
                        }
                    }
                }
                index = 0
            }

            
            
           // Cambio de fuente y formateo de párrafo
           accionesModificada.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17), range: NSRange(location: 0, length: accionesModificada.length))
        
      
        
          heparinizacionLentaModificada.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17), range: NSRange(location: 0, length: heparinizacionLentaModificada.length))
         heparinizacionRapidaModificada.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17), range: NSRange(location: 0, length: heparinizacionRapidaModificada.length))
           
            let estiloParrafo = NSMutableParagraphStyle()
            estiloParrafo.alignment = .center // center the text
            estiloParrafo.lineSpacing = 1 //Change spacing between lines
            //estiloParrafo.paragraphSpacing = 38 //Change space between paragraphs
            accionesModificada.addAttributes([.paragraphStyle: estiloParrafo], range: NSRange(location: 0, length: accionesModificada.length))
          heparinizacionLentaModificada.addAttributes([.paragraphStyle: estiloParrafo], range: NSRange(location: 0, length: heparinizacionLentaModificada.length))
          heparinizacionRapidaModificada.addAttributes([.paragraphStyle: estiloParrafo], range: NSRange(location: 0, length: heparinizacionRapidaModificada.length))
            protocoloTextView.attributedText = accionesModificada
          heparinizacionLentaTextView.attributedText = heparinizacionLentaModificada
        heparinizacionRapidaTextView.attributedText = heparinizacionRapidaModificada
       
    redimensionarTexView()
    }
   
    // Requiere UITextViewDelegate. Desmarcar en la vista de propiedades Editable y ScrollingEnabled
    func redimensionarTexView() {
        heparinizacionRapidaTextView!.delegate = self
        var anchoFijo = heparinizacionRapidaTextView!.frame.size.width
        var nuevoTamaño: CGSize = heparinizacionRapidaTextView!.sizeThatFits(CGSize(width: anchoFijo, height: CGFloat(MAXFLOAT)))
        var nuevoMarco = heparinizacionRapidaTextView.frame
        // nuevoMarco.size = CGSize(width: CGFloat(fmaxf(Float(nuevoTamaño.width), Float(anchoFijo))), height: nuevoTamaño.height)
        nuevoMarco.size = CGSize(width: anchoFijo, height: nuevoTamaño.height)
        heparinizacionRapidaTextView.frame = nuevoMarco
        
        heparinizacionLentaTextView!.delegate = self
        anchoFijo = heparinizacionLentaTextView!.frame.size.width
        nuevoTamaño = heparinizacionLentaTextView!.sizeThatFits(CGSize(width: anchoFijo, height: CGFloat(MAXFLOAT)))
        nuevoMarco = heparinizacionLentaTextView.frame
        // nuevoMarco.size = CGSize(width: CGFloat(fmaxf(Float(nuevoTamaño.width), Float(anchoFijo))), height: nuevoTamaño.height)
        nuevoMarco.size = CGSize(width: anchoFijo, height: nuevoTamaño.height)
        heparinizacionLentaTextView.frame = nuevoMarco
        
        protocoloTextView!.delegate = self
        anchoFijo = protocoloTextView!.frame.size.width
        nuevoTamaño = protocoloTextView!.sizeThatFits(CGSize(width: anchoFijo, height: CGFloat(MAXFLOAT)))
        nuevoMarco = protocoloTextView.frame
        // nuevoMarco.size = CGSize(width: CGFloat(fmaxf(Float(nuevoTamaño.width), Float(anchoFijo))), height: nuevoTamaño.height)
        nuevoMarco.size = CGSize(width: anchoFijo, height: nuevoTamaño.height)
        protocoloTextView.frame = nuevoMarco
        
        anotacionesTextView!.delegate = self
        anchoFijo = anotacionesTextView!.frame.size.width
        nuevoTamaño = anotacionesTextView!.sizeThatFits(CGSize(width: anchoFijo, height: CGFloat(MAXFLOAT)))
        nuevoMarco = anotacionesTextView.frame
        // nuevoMarco.size = CGSize(width: CGFloat(fmaxf(Float(nuevoTamaño.width), Float(anchoFijo))), height: nuevoTamaño.height)
        nuevoMarco.size = CGSize(width: anchoFijo, height: nuevoTamaño.height)
        anotacionesTextView.frame = nuevoMarco
        
        
    }
        
        
        
        
        
        
        
    }



