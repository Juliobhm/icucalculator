//
//  NutricionViewController.swift
//  Nutricion01
//
//  Created by Julio Barado Hualde on 06/02/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit
class NutricionViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet var PrincipalView: UIView!
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var marcoSliderView: UIView!
   
    @IBOutlet weak var StackView123: UIStackView!
    @IBOutlet weak var label01: UILabel!
    @IBOutlet weak var label02: UILabel!
    @IBOutlet weak var label03: UILabel!
    @IBOutlet weak var StackView456: UIStackView!
    @IBOutlet weak var label04: UILabel!
    @IBOutlet weak var label05: UILabel!
    @IBOutlet weak var label06: UILabel!
    
    
    var secciones = ["Datos fisiológicos", "Valores  necesarios", "Calorias", "Proteinas"]
    var etiquetas = [["Edad (años)", "Peso actual (kg)", "Peso elegido: ", "Talla (cm)", "Sexo", "Gasto energético basal"],
                     ["Hidratos de carbono (g)", "Lípidos (g)", "Nitrógeno (g)"],
                     ["Calorías Totales", "Calorías / kg", "Porcentaje del GEB", "Calorías No Proteicas", "Porcentaje HdC / Lípidos"],
                     ["Calorías NP / gramo N2", "Gramos de proteína por kg"]]
    var valores = [["Edad (años)", "Peso actual (kg)", "Peso elegido: ", "Talla (cm)", "Sexo", "Gasto energético basal"],
                   ["Hidratos de carbono (g)", "Lípidos (g)", "Nitrógeno (g)"],
                   ["Calorías Totales", "Calorías / kg", "Porcentaje del GEB", "Calorías No Proteicas", "Porcentaje HdC / Lípidos"],
                   ["Calorías NP / gramo N2", "Gramos de proteína por kg"]]
    
    
    var slider0 = UISlider()
    var slider1 = UISlider()
    
    var maximoValorSlider0: Float = 100
    var minimoValorSlider0: Float = 0
    var maximoValorSlider1: Float = 100
    var minimoValorSlider1: Float = 0
    var marcadorCampo: String = ""
    var slider0Posición: Float = 0
    var slider1Posición: Float = 0
    
    var pesoElegido: Float = 0
    var pesoElegido0: Float = 0
    
    var pesoActual: Float = 70
    var pesoIdeal: Float = 70
    var pesoAjustado: Float = 70
    var pesoCalculos: Float = 70
    var talla: Float = 170
    var edad: Float = 50
    var sexo: String = "Hombre"
    
    var edad0: Float = 50
    var edad1: Float = 0
    
    var pesoActual0: Float = 70
    var pesoActual1: Float = 0
    
    var pesoCalculos0: Float = 0
    var pesoCalculos1: Float = 0
    
    var talla0: Float = 170
    var talla1: Float = 0
    
    var sexo0: Float = 0
    
    var harrisBenedict: Float = 1
    
    var nitrogeno: Float = 10
    var nitrogeno0: Float = 10
    var nitrogeno1: Float = 0
    
    var caloriasTotales: Float = 0
    var caloriasTotales0: Float = 0
    var caloriasTotales1: Float = 0
    
    var caloriasNoProteicas: Float = 0
    var caloriasNoProteicas0: Float = 0
    var caloriasNoProteicas1: Float = 0
    
    var calKg: Float = 0
    var calKg0: Float = 0
    var calKg1: Float = 0
    
    var caloriasBasal: Float = 0
    var calHarris: Float = 1
    var calHarris0: Float = 1
    var calHarris1: Float = 0
    
    var HdeCLipidos: Float = 50
    var HdeCLipidos0: Float = 5
    var HdeCLipidos1: Float = 0
    
    var HdeC: Float = 0
    var HdeC0: Float = 0
    var HdeC1: Float = 0
    
    var lipidos: Float = 0
    var lipidos0: Float = 0
    var lipidos1: Float = 0
    
    var calNPgN2: Float = 0
    var calNPgN20: Float = 0
    var calNPgN21: Float = 0
    
    var gramosProtKg: Float = 0
    var gramosProtKg0: Float = 0
    var gramosProtKg1: Float = 0
    
    var indiceActual: IndexPath = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        pesoCalculos = pesoActual
        marcadorCampo = "calHarrisTF"
        calculoPeso()
        calculoParametos()
        marcadorCampo = "nitrogenoTF"
        calculoParametos()
        
        slider()

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return secciones[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return etiquetas[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // if etiquetas[indexPath.section][indexPath.row] == etiquetaSeleccionada {
        if indexPath == indiceActual {
            let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSeleccionada", for: indexPath) as! CeldaSeleccionada
            celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
            celda.valores.text = valores[indexPath.section][indexPath.row]
            
            return celda
        }
        else {
            let celda = tableView.dequeueReusableCell(withIdentifier: "MiCelda", for: indexPath) as! CeldasView
            celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
            celda.valores.text = valores[indexPath.section][indexPath.row]
            return celda
        }
        
    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        slider0.isEnabled = true
        slider0.alpha = 1
        slider1.isEnabled = true
        slider1.alpha = 1
        
        indiceActual = tabla.indexPathForSelectedRow ?? [0,0]
        tabla.reloadData()
        
        switch (indexPath.section, indexPath.row){
        case (0, 0):
            marcadorCampo = "edadTF"
            minimoValorSlider0 = 14
            maximoValorSlider0 = 99
            minimoValorSlider1 = 0.0
            maximoValorSlider1 = 0.9
            slider0Posición = edad0
            slider1Posición = edad1
            label01.text = "14"
            label02.text = ""
            label03.text = "99"
            label04.text = "0.0"
            label05.text = ""
            label06.text = "0.9"
            
        case (0, 1):
            marcadorCampo = "pesoActualTF"
            minimoValorSlider0 = 30
            maximoValorSlider0 = 140
            minimoValorSlider1 = 0.0
            maximoValorSlider1 = 0.9
            slider0Posición = pesoActual0
            slider1Posición = pesoActual1
            label01.text = "30"
            label02.text = ""
            label03.text = "140"
            label04.text = "0.0"
            label05.text = ""
            label06.text = "0.9"
            
        case (0, 2):
            marcadorCampo = "pesoElegidoTF"
            minimoValorSlider0 = 0
            maximoValorSlider0 = 2
            minimoValorSlider1 = 0
            maximoValorSlider1 = 2
            slider0Posición = pesoElegido0
            slider1Posición = pesoElegido0
            label01.text = "Peso Actual"
            label02.text = "Peso Ideal"
            label03.text = "Peso Ajustado"
            label04.text = ""
            label05.text = ""
            label06.text = ""
            slider1Posición = 0
            slider1.isEnabled = false
            slider1.alpha = 0.2
            
        case (0, 3):
            marcadorCampo = "tallaTF"
            minimoValorSlider0 = 70
            maximoValorSlider0 = 220
            minimoValorSlider1 = 0.0
            maximoValorSlider1 = 0.9
            slider0Posición = talla0
            slider1Posición = talla1
            label01.text = "70"
            label02.text = ""
            label03.text = "220"
            label04.text = "0.0"
            label05.text = ""
            label06.text = "0.9"
            
        case (0, 4):
            marcadorCampo = "sexoTF"
            minimoValorSlider0 = 0
            maximoValorSlider0 = 1
            minimoValorSlider1 = 0
            maximoValorSlider1 = 1
            slider0Posición = sexo0
            slider1Posición = sexo0
            label01.text = "Hombre"
            label02.text = ""
            label03.text = "Mujer"
            label04.text = ""
            label05.text = ""
            label06.text = ""
            slider1Posición = 0
            slider1.isEnabled = false
            slider1.alpha = 0.2
            
        case (0, 5):
            label01.text = ""
            label02.text = ""
            label03.text = ""
            label04.text = ""
            label05.text = ""
            label06.text = ""
            slider0Posición = 0
            slider0.isEnabled = false
            slider1Posición = 0
            slider1.isEnabled = false
            
        case (1, 0):
            marcadorCampo = "HdeCTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = HdeC0
            slider1Posición = HdeC1
            label01.text = "20"
            label02.text = ""
            label03.text = "390"
            label04.text = "0"
            label05.text = ""
            label06.text = "9"
            
        case (1, 1):
            marcadorCampo = "lipidosTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = lipidos0
            slider1Posición = lipidos1
            label01.text = "20"
            label02.text = ""
            label03.text = "390"
            label04.text = "0"
            label05.text = ""
            label06.text = "9"
            
        case (1, 2):
            marcadorCampo = "nitrogenoTF"
            minimoValorSlider0 = 5
            maximoValorSlider0 = 30
            minimoValorSlider1 = 0
            maximoValorSlider1 = 0.9
            slider0Posición = nitrogeno0
            slider1Posición = nitrogeno1
            label01.text = "5"
            label02.text = ""
            label03.text = "30"
            label04.text = "0.0"
            label05.text = ""
            label06.text = "0.9"
            
        case (2, 0):
            marcadorCampo = "calTotalesTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 19
            slider0Posición = caloriasNoProteicas0
            slider1Posición = caloriasNoProteicas1
            label01.text = ""
            label02.text = "Incrementos de 100 en 100"
            label03.text = ""
            label04.text = ""
            label05.text = "Incrementos de 5 en 5"
            label06.text = ""
            
        case (2, 1):
            marcadorCampo = "calKgTF"
            minimoValorSlider0 = 5
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0.0
            maximoValorSlider1 = 0.9
            slider0Posición = calKg0
            slider1Posición = calKg1
            label01.text = "5"
            label02.text = ""
            label03.text = "39"
            label04.text = "0"
            label05.text = ""
            label06.text = "9"
            
        case (2, 2):
            marcadorCampo = "calHarrisTF"
            minimoValorSlider0 = 0.1
            maximoValorSlider0 = 3
            minimoValorSlider1 = 0.00
            maximoValorSlider1 = 0.09
            slider0Posición = calHarris0
            slider1Posición = calHarris1
            label01.text = "0.1"
            label02.text = ""
            label03.text = "3"
            label04.text = "0.00"
            label05.text = ""
            label06.text = "0.09"
            
        case (2, 3):
            marcadorCampo = "calNoProteicaTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 19
            slider0Posición = caloriasNoProteicas0
            slider1Posición = caloriasNoProteicas1
            label01.text = ""
            label02.text = "Incrementos de 100 en 100"
            label03.text = ""
            label04.text = ""
            label05.text = "Incrementos de 5 en 5"
            label06.text = ""
            
        case (2, 4):
            marcadorCampo = "HdeCLipidosTF"
            minimoValorSlider0 = 0
            maximoValorSlider0 = 9
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = HdeCLipidos0
            slider1Posición = HdeCLipidos1
            label01.text = "0"
            label02.text = ""
            label03.text = "90"
            label04.text = "0"
            label05.text = ""
            label06.text = "9"
            
            
        case (3, 0):
            marcadorCampo = "calNPgN2TF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 29
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = calNPgN20
            slider1Posición = calNPgN21
            label01.text = "10"
            label02.text = ""
            label03.text = "290"
            label04.text = "0"
            label05.text = ""
            label06.text = "9"
            
            
        case (3, 1):
            marcadorCampo = "gramosProtKgTF"
            minimoValorSlider0 = 0.2
            maximoValorSlider0 = 3.9
            minimoValorSlider1 = 0.00
            maximoValorSlider1 = 0.09
            slider0Posición = gramosProtKg0
            slider1Posición = gramosProtKg1
            label01.text = "0.2"
            label02.text = ""
            label03.text = "4"
            label04.text = "0.00"
            label05.text = ""
            label06.text = "0.09"
            
        default:
            break
        }
        
        slider()
    }
 
    
    func slider(){
        self.slider0.removeFromSuperview()
        self.marcoSliderView.addSubview(slider0)
        //slider0 = UISlider(frame:CGRect(x: 0, y: 0, width: 0, height: 0))
        slider0.translatesAutoresizingMaskIntoConstraints = false
        
        slider0.minimumValue = minimoValorSlider0
        slider0.maximumValue = maximoValorSlider0
        slider0.isContinuous = true
        slider0.tintColor = UIColor.green
        slider0.addTarget(self, action: #selector(NutricionViewController.sliderValueDidChange(_:)), for: .valueChanged)
        self.marcoSliderView.addSubview(slider0)
        slider0.setValue(slider0Posición, animated: true)
        
        self.slider1.removeFromSuperview()
        self.marcoSliderView.addSubview(slider1)
        //slider1 = UISlider(frame:CGRect(x: 0, y: 0, width: 0, height: 0))
        
        slider1.translatesAutoresizingMaskIntoConstraints = false
        
        slider1.minimumValue = minimoValorSlider1
        slider1.maximumValue = maximoValorSlider1
        slider1.isContinuous = true
        slider1.tintColor = UIColor.green
        slider1.addTarget(self, action: #selector(NutricionViewController.sliderValueDidChange(_:)), for: .valueChanged)
        self.marcoSliderView.addSubview(slider1)
        slider1.setValue(slider1Posición, animated: true)
        
        slider0.leadingAnchor.constraint(equalTo: marcoSliderView.leadingAnchor, constant: 10).isActive = true
        slider0.trailingAnchor.constraint(equalTo: marcoSliderView.trailingAnchor, constant: -10).isActive = true
        slider0.topAnchor.constraint(equalTo: marcoSliderView.topAnchor, constant: 25).isActive = true
        
        slider1.leadingAnchor.constraint(equalTo: marcoSliderView.leadingAnchor, constant: 10).isActive = true
        slider1.trailingAnchor.constraint(equalTo: marcoSliderView.trailingAnchor, constant: -10).isActive = true
        slider1.bottomAnchor.constraint(equalTo: marcoSliderView.bottomAnchor, constant: -15).isActive = true
        
        StackView123.translatesAutoresizingMaskIntoConstraints = false
        StackView123.leadingAnchor.constraint(equalTo: marcoSliderView.leadingAnchor, constant: 10).isActive = true
        StackView123.trailingAnchor.constraint(equalTo: marcoSliderView.trailingAnchor, constant: -10).isActive = true
        StackView123.topAnchor.constraint(equalTo: marcoSliderView.topAnchor, constant: 10).isActive = true
        
        StackView456.translatesAutoresizingMaskIntoConstraints = false
        StackView456.leadingAnchor.constraint(equalTo: marcoSliderView.leadingAnchor, constant: 10).isActive = true
        StackView456.trailingAnchor.constraint(equalTo: marcoSliderView.trailingAnchor, constant: -10).isActive = true
        StackView456.bottomAnchor.constraint(equalTo: marcoSliderView.bottomAnchor, constant: -38).isActive = true
    }
    
    
    func calculoHarrisBenedict(peso: Float, talla: Float, edad: Float, sexo: String) -> Float{
        
        if (pesoCalculos != nil && talla != nil && edad != nil && sexo != nil){
            switch sexo {
            case "Hombre":
                caloriasBasal = 66.47 + (13.75 * pesoCalculos) + (5 * talla) - (6.75 * edad)
            case "Mujer":
                caloriasBasal = 665.1 + (9.563 * pesoCalculos) + (1.85 * talla) - (4.676 * edad)
            default:
                break
            }
        }
        /*The versions of the HB equation for men and
         women are as follows, with W equal to weight in kilograms,
         H the height in centimeters, and A the age in
         years: 66.47 + (13.75 × W) + (5 × H) – (6.75 × A) for men
         and 665.1 + (9.563 × W) + (1.85 × H) – (4.676 × A) for
         women. The ideal body weight was calculated from
         the height in meters: 22.5 × H2 for men and 21.5 × H2
         for women. Predicted body weight was calculated
         from the height in centimeters: 50 + 0.91 (H – 152.4)
         for men and 45.5 + 0.91 (H – 152.4) for women.
         */
        //caloriasBasal = redondeoCinco(numero: caloriasBasal)
        return caloriasBasal
    }
    
    
    func separarNumeros(numero: Float, indice01: Float, indice02: Float) -> (Float, Float) {
        var primerValor = Float(Int(numero * indice01))
        let primero = (primerValor / indice01)
        let diferencia = numero - primero
        var segundoValor = (round(diferencia * indice02)) / indice02
        //print("Primer valor Previo:  \(primerValor). Segundo valor Previo:  \(segundoValor)")
        
        if (segundoValor * 10 / indice02) == 1 {
            segundoValor = 0
            primerValor = primerValor + 1
        }
        // print("Primer valor:  \(primerValor). Segundo valor:  \(segundoValor)")
        
        return (primerValor ?? 0.0, segundoValor ?? 0.0)
    }
    
    func redondeoCinco(numero: Float) -> Float{
        var valorRedondeado = numero
        let resto = round(numero / 10) * 10
        let diferencia = numero - resto
        switch diferencia {
        case 0..<2.5:
            valorRedondeado = resto
        case 2.5..<5:
            valorRedondeado = resto + 5
        case -5..<(-2.5):
            valorRedondeado = resto - 5
        default:
            valorRedondeado = resto
        }
        print ("Numero recibido: \(numero). Valor redondeado: \(valorRedondeado). Diferencia: \(diferencia)Numero devuelto: \(valorRedondeado)")
        
        return (valorRedondeado)
        
        
    }
    func calculoPeso(){
        pesoActual = pesoActual0 + pesoActual1
        pesoElegido = pesoElegido0
        pesoIdeal = ((talla / 2.54) - 60) * 2.3 + 45.5
        //let limiteObesidad = pesoIdeal * 1.3
        pesoAjustado = pesoIdeal + ((pesoActual - pesoIdeal) * 0.25)
        if pesoElegido0 == 0 {
            pesoCalculos = pesoActual}
        else if pesoElegido0 == 1 {
            pesoCalculos = pesoIdeal}
        else {
            pesoCalculos = pesoAjustado}
        
        let respuesta = separarNumeros(numero: pesoCalculos, indice01: 1, indice02: 10)
        pesoCalculos0 = respuesta.0
        pesoCalculos1 = respuesta.1
        
        
        //  pesoCalculos0 = separarEnteroDecimal(numero: pesoCalculos).0
        // pesoCalculos1 = round(separarEnteroDecimal(numero: pesoCalculos).1 * 10) / 10
        
        // calculoPeso(peso: pesoActual, talla: talla, pesoElegido: pesoElegido)
        
        edad = edad0 + edad1
        talla = talla0 + talla1
        if sexo0 == 0 {
            sexo = "Hombre"}
        else{
            sexo = "Mujer"}
        caloriasBasal = calculoHarrisBenedict(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
        caloriasBasal = redondeoCinco(numero: caloriasBasal)
        
        
    }
    
    func calculoCaloriasTotales(){
        caloriasTotales = redondeoCinco(numero: calKg * pesoCalculos)
        
        let respuesta = separarNumeros(numero: caloriasTotales, indice01: 0.01, indice02: 1)
        caloriasTotales0 = respuesta.0
        caloriasTotales1 = round(respuesta.1 / 5)
    }
    
    func calculoCaloriasNoProteicas(){
        caloriasNoProteicas = redondeoCinco(numero: calKg * pesoCalculos - nitrogeno * 6.25 * 4)
        
        let respuesta = separarNumeros(numero: caloriasNoProteicas, indice01: 0.01, indice02: 1)
        caloriasNoProteicas0 = respuesta.0
        caloriasNoProteicas1 = round(respuesta.1 / 5)
    }
    
    
    
    func calculoCalKg(){
        calKg = caloriasTotales / pesoCalculos
        let respuesta = separarNumeros(numero: calKg, indice01: 1, indice02: 10)
        calKg0 = respuesta.0
        calKg1 = respuesta.1
    }
    
    func calculoHarrisBenedic(){
        //calHarris = round (caloriasNoProteicas * 100 / caloriasBasal) / 100
        calHarris = caloriasTotales / caloriasBasal
        let respuesta = separarNumeros(numero: calHarris, indice01: 10, indice02: 100)
        calHarris0 =  respuesta.0 / 10
        calHarris1 =  respuesta.1
    }
    
    func calculoHdeC(){
        HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
        let respuesta = separarNumeros(numero: HdeC, indice01: 0.1, indice02: 1)
        HdeC0 = respuesta.0
        HdeC1 = respuesta.1
    }
    
    func calculoLipidos(){
        lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
        let respuesta = separarNumeros(numero: lipidos, indice01: 0.1, indice02: 1)
        lipidos0 = respuesta.0
        lipidos1 = respuesta.1
    }
    
    func calculoCalNPgN2(){
        calNPgN2 = caloriasNoProteicas / nitrogeno
        let respuesta = separarNumeros(numero: calNPgN2, indice01: 0.1, indice02: 1)
        calNPgN20 = respuesta.0
        calNPgN21 = respuesta.1
    }
    
    func calculoHdeCLipidos(){
        HdeCLipidos = HdeC * 4 * 100 / caloriasNoProteicas
        let respuesta = separarNumeros(numero: HdeCLipidos, indice01: 0.1, indice02: 1)
        HdeCLipidos0 = respuesta.0
        HdeCLipidos1 = respuesta.1
    }
    
    func calculoNitrogeno(){
        nitrogeno = pesoCalculos * gramosProtKg / 6.25
        let respuesta = separarNumeros(numero: nitrogeno, indice01: 1, indice02: 10)
        nitrogeno0 = respuesta.0
        nitrogeno1 = respuesta.1
    }
    
    func calculoGramosProtKg(){
        gramosProtKg = nitrogeno * 6.25 / pesoCalculos
        let respuesta = separarNumeros(numero: gramosProtKg, indice01: 10, indice02: 100)
        gramosProtKg0 = respuesta.0 / 10
        gramosProtKg1 = respuesta.1
    }
    
    func calculoParametos(){
        
        switch marcadorCampo {
            
        case "pesoActualTF", "pesoElegidoTF", "tallaTF", "edadTF", "sexoTF":
            calculoPeso()
            calculoCalKg()
            calculoHarrisBenedic()
            calculoGramosProtKg()
            
        case "calTotalesTF":
            caloriasTotales = caloriasTotales0 * 100 + caloriasTotales1 * 5
            //caloriasNoProteicas = caloriasTotales - nitrogeno * 6.25 * 4
            calculoCaloriasNoProteicas()
            calculoCalKg()
            calculoHarrisBenedic()
            calculoHdeC()
            calculoLipidos()
            calculoCalNPgN2()
            
        case "calNoProteicaTF":
            caloriasNoProteicas = caloriasNoProteicas0 * 100 + caloriasNoProteicas1 * 5
            //caloriasTotales = caloriasNoProteicas + nitrogeno * 6.25 * 4
            calculoCaloriasTotales()
            calculoCalKg()
            calculoHarrisBenedic()
            calculoHdeC()
            calculoLipidos()
            calculoCalNPgN2()
            
        case "calKgTF":
            calKg = calKg0 + calKg1
            calculoCaloriasTotales()
            calculoCaloriasNoProteicas()
            calculoHarrisBenedic()
            calculoHdeC()
            calculoLipidos()
            calculoCalNPgN2()
            
        case "calHarrisTF":
            calHarris = calHarris0 + calHarris1
            calKg = caloriasBasal * calHarris / pesoCalculos
            calculoCaloriasTotales()
            calculoCaloriasNoProteicas()
            calculoCalKg()
            calculoHdeC()
            calculoLipidos()
            calculoCalNPgN2()
            
        case "HdeCLipidosTF":
            HdeCLipidos = (HdeCLipidos0 * 10 + HdeCLipidos1)
            calculoHdeC()
            calculoLipidos()
            
        case "HdeCTF":
            HdeC = HdeC0 * 10 + HdeC1
            caloriasNoProteicas = HdeC * 4 + lipidos * 9
            calKg = (HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4) / pesoCalculos
            calculoCaloriasTotales()
            calculoCaloriasNoProteicas()
            calculoHarrisBenedic()
            calculoHdeCLipidos()
            calculoCalNPgN2()
            calculoCalKg()
            
        case "lipidosTF":
            lipidos = lipidos0 * 10 + lipidos1
            calKg = (HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4) / pesoCalculos
            calKg = (HdeC * 4 + lipidos * 9) / pesoCalculos
            calculoCaloriasTotales()
            calculoCaloriasNoProteicas()
            calculoHarrisBenedic()
            calculoHdeCLipidos()
            calculoCalNPgN2()
            calculoCalKg()
            
        case "nitrogenoTF":
            nitrogeno = nitrogeno0 + nitrogeno1
            calKg = (HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4) / pesoCalculos
            calculoCalNPgN2()
            calculoGramosProtKg()
            calculoCaloriasNoProteicas()
            calculoCaloriasTotales()
            calculoHarrisBenedic()
            calculoCalKg()
            
        case "calNPgN2TF":
            calNPgN2 = calNPgN20 * 10 + calNPgN21
            caloriasNoProteicas = calNPgN2 * nitrogeno
            calculoHdeC()
            calculoLipidos()
            calKg = (HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4) / pesoCalculos
            calculoCaloriasNoProteicas()
            calculoCaloriasTotales()
            calculoHarrisBenedic()
            calculoCalKg()
            
        case "gramosProtKgTF":
            gramosProtKg = gramosProtKg0 + gramosProtKg1
            calculoNitrogeno()
            calKg = (HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4) / pesoCalculos
            calculoCalNPgN2()
            calculoGramosProtKg()
            calculoCaloriasNoProteicas()
            calculoCaloriasTotales()
            calculoHarrisBenedic()
            calculoCalKg()
            calculoCalNPgN2()
            
        default:
            break
            
        }
        rellenarCampos()
        
    }
    func rellenarCampos(){
        valores[0][0] = String(format: "%.1f", (edad0 + edad1))
        
        valores[0][1] = String(format: "%.1f", (pesoActual0 + pesoActual1))
        valores[0][2] = String(format: "%.1f", pesoCalculos0 + pesoCalculos1)
        if pesoElegido0 == 0 {
            etiquetas[0][2] = "Peso elegido: Peso Actual"}
        else if pesoElegido0 == 1 {
            etiquetas[0][2] = "Peso elegido: Peso Ideal"}
        else {
            etiquetas[0][2] = "Peso elegido: Peso Ajustado"}
        
        
        valores[0][3] = String(format: "%.1f", (talla0 + talla1))
        valores[0][4] = sexo
        valores[0][5] = String(format: "%.0f", (caloriasBasal))
        
        
        valores[2][0] = String(format: "%.0f", (caloriasTotales0 * 100 + caloriasTotales1 * 5))
        valores[2][1] = String(format: "%.1f", (calKg0 + calKg1))
        valores[2][2] = String(format: "%.2f", (calHarris0 + calHarris1))
        valores[2][3] = String(format: "%.0f", (caloriasNoProteicas0 * 100 + caloriasNoProteicas1 * 5))
        valores[2][4] = String(format: "%.0f", (HdeCLipidos0 * 10 + HdeCLipidos1))
        
        valores[1][0] = String(format: "%.0f", (HdeC0 * 10 + HdeC1))
        valores[1][1] = String(format: "%.0f", (lipidos0 * 10 + lipidos1))
        valores[1][2] = String(format: "%.1f", (nitrogeno0 + nitrogeno1))
        valores[3][0] = String(format: "%.0f", (calNPgN20 * 10 + calNPgN21))
        valores[3][1] = String(format: "%.2f", (gramosProtKg0 + gramosProtKg1))
        /*
         print("GEB: \(caloriasBasal)")
         print("Calorias NP: \(caloriasNoProteicas), Calorias0: \(caloriasNoProteicas0), Calorias1: \(caloriasNoProteicas1)")
         print("Calorias Totales: \(caloriasTotales), CaloriasTotales0: \(caloriasTotales0), CaloriasTotales1: \(caloriasTotales1)")
         
         print("Calorias kilo: \(calKg)")
         print("Calorias Harris: \(calHarris), calharris0: \(calHarris0), calharris1: \(calHarris1)")
         print("HdeC: \(HdeC)")
         print("Lipidos: \(lipidos)")
         print("Calorias nitrogeno: \(calNPgN2)")
         print("Lipidos: \(lipidos). Lipidos0: \(lipidos0). Lipidos1: \(lipidos1).")
         print("Peso: \(pesoCalculos). Peso0: \(pesoCalculos0). Peso1: \(pesoCalculos1).")
         */
        tabla.reloadData()
        
        
    }
    
    
    // @objc func slidervaluewillchange(_ sender:UISlider){
    
    @objc func sliderValueDidChange(_ sender:UISlider!){
        
        switch marcadorCampo {
        case "edadTF":
            edad0 = round(slider0.value)
            edad1 = round(slider1.value * 10) / 10
            
        case "pesoActualTF":
            pesoActual0 = round(slider0.value)
            pesoActual1 = round(slider1.value * 10) / 10
            
        case "tallaTF":
            talla0 = round(slider0.value)
            talla1 = round(slider1.value * 10) / 10
            
        case "pesoElegidoTF":
            self.slider0.value = round(self.slider0.value)
            pesoElegido0 = slider0.value
            
        case "sexoTF":
            self.slider0.value = round(self.slider0.value)
            sexo0 = slider0.value
            //sexo0 = round(slider0.value)
            
        case "calNoProteicaTF":
            caloriasNoProteicas0 = round(slider0.value)
            caloriasNoProteicas1 = round(slider1.value)
            
        case "calTotalesTF":
            caloriasTotales0 = round(slider0.value)
            caloriasTotales1 = round(slider1.value)
            
        case "calKgTF":
            calKg0 = round(slider0.value)
            calKg1 = round(slider1.value * 10) / 10
            
        case "calHarrisTF":
            calHarris0 = round(slider0.value * 10) / 10
            calHarris1 = round(slider1.value * 100) / 100
            
        case "HdeCTF":
            HdeC0 = round(slider0.value)
            HdeC1 = round(slider1.value)
            
        case "lipidosTF":
            lipidos0 = round(slider0.value)
            lipidos1 = round(slider1.value)
            
        case "HdeCLipidosTF":
            HdeCLipidos0 = round(slider0.value)
            HdeCLipidos1 = round(slider1.value)
            
        case "nitrogenoTF":
            nitrogeno0 = round(slider0.value)
            nitrogeno1 = round(slider1.value * 10) / 10
            
        case "calNPgN2TF":
            calNPgN20 = round(slider0.value)
            calNPgN21 = round(slider1.value)
            
        case "gramosProtKgTF":
            gramosProtKg0 = round(slider0.value * 10) / 10
            gramosProtKg1 = round(slider1.value * 100) / 100
            
        default:
            break
        }
        
        calculoParametos()
    }
 
 

}

