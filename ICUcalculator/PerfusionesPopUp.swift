//
//  PerfusionesPopUp.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 15/12/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class PerfusionesPopUp: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var nombreTF: UITextField!
    @IBOutlet var cantidadTF: UITextField!
    @IBOutlet weak var unidadMedidaTF: UITextField!
    @IBOutlet var volumenFarmTF: UITextField!
    @IBOutlet var volumenDisolTF: UITextField!
    @IBOutlet weak var unidadDosificacionTF: UITextField!
    @IBOutlet var dosisRecoTF: UITextField!
    
    @IBOutlet weak var dosisRecoTV: UITextView!
    @IBOutlet var marcoAvisos: UIView!
    @IBOutlet var avisosLabel: UILabel!
    @IBOutlet var aceptarAvisosButton: UIButton!
    @IBOutlet var salirAvisosButton: UIButton!
    var picker01 = UIPickerView()
    var picker02 = UIPickerView()
    var picker03 = UIPickerView()
    var barra02 = UIToolbar()
    @IBOutlet var contenedorView: UIView!
    @IBOutlet var marcoValores: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var marcoBotones: UIView!
    @IBOutlet var marcoPicker01: UIView!
    @IBOutlet var marcoPicker03: UIView!
    
    
    @IBOutlet weak var textView01: UITextView!
    
    
    
    var activoTextField = UITextField()

    @IBOutlet weak var nuevoFarmacoButton: UIButton!
    
    class Farmaco: Codable {
        var nombre: String
        var cantidad: Double
        var picker02Indice01: Int=0
        var volumenFarmaco: Double
        var volumenDisolucion: Double
        var picker03Indice01: Int = 0
        var picker03Indice02: Int = 0
        var picker03Indice03: Int = 0
        var dosisRecomendada: String
        
        init(nombre : String, cantidad: Double, picker02Indice01: Int, volumenFarmaco: Double, volumenDisolucion: Double, dosisRecomendada: String, picker03Indice01: Int, picker03Indice02: Int, picker03Indice03: Int) {
            self.nombre = nombre
            self.cantidad = cantidad
            self.picker02Indice01 = picker02Indice01
            self.volumenFarmaco = volumenFarmaco
            self.volumenDisolucion = volumenDisolucion
            self.picker03Indice01 = picker03Indice01
            self.picker03Indice02 = picker03Indice02
            self.picker03Indice03 = picker03Indice03
            self.dosisRecomendada = dosisRecomendada
        }
    }
    var listaFarmacos: [Farmaco] = []
    var unidad = ["","mcg", "mg", "g"]
    var unidadReferencia = ["", "kg"]
    var tiempo = ["","minuto", "hora", "día"]
    var posicion: Int!
    var posicionPicker: Int = 0
    var accionRealizar: Int = 0
    var unidadValida: Bool = false
    var unidadPrimeraVez: Bool = true
    var unidadMedidaPrimeraVez: Bool = true


    var verdeInferiorColor: CGColor = UIColor(red: 51/255, green: 204/255, blue: 0/255, alpha: 1.0).cgColor
    var verdeSuperiorColor: CGColor = UIColor(red: 4/255, green: 206/255, blue: 44/255, alpha: 1.0).cgColor
    var amarilloInferiorColor: CGColor = UIColor(red: 200/255, green: 204/255, blue: 0/255, alpha: 1.0).cgColor
    var amarilloSuperiorColor: CGColor = UIColor(red: 252/255, green: 249/255, blue: 60/255, alpha: 1.0).cgColor
    var naranjaInferiorColor: CGColor = UIColor(red: 214/255, green: 146/255, blue: 51/255, alpha: 1.0).cgColor
    var naranjaSuperiorColor: CGColor = UIColor(red: 255/255, green: 127/255, blue: 0/255, alpha: 1.0).cgColor
    var rojoInferiorColor: CGColor = UIColor(red: 214/255, green: 146/255, blue: 51/255, alpha: 1.0).cgColor
    var rojoSuperiorColor: CGColor = UIColor(red: 252/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
    
    var colorAzul: UIColor = UIColor(red: 0/255, green: 74/255, blue: 211/255, alpha: 1.0)
    var colorFondoPicker: UIColor = UIColor(red: 173/255, green: 218/255, blue: 247/255, alpha: 1.0)
    var colorGris: UIColor = UIColor(red: 237/255, green: 234/255, blue: 222/255, alpha: 1.0)
    
    func viewForZooming(in ScrollView: UIScrollView) -> UIView? {
        return contenedorView}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mostrarAnimacion(vista: view)
        marcoAvisos.isHidden = true
        picker01.delegate = self
        picker01.dataSource = self
        picker02.delegate = self
        picker02.dataSource = self
        picker03.delegate = self
        picker03.dataSource = self
        
        let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
        let lista = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
        listaFarmacos = lista
       
        
        // Procedimiento zooom, requiere class UIScrollViewDelegate y llamada a lafunción viewForZooming
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        scrollView.contentSize.height = 1000
        // Fin método Pinch zoom"

        dosisRecoTV.layer.borderColor = UIColor.lightGray.cgColor
        dosisRecoTV.layer.borderWidth = 1.0;
        dosisRecoTV.layer.cornerRadius = 5.0;
        self.marcoPicker01.addSubview(picker01)
       
       
        picker01.translatesAutoresizingMaskIntoConstraints = false
        picker01.leadingAnchor.constraint(equalTo: marcoPicker01.leadingAnchor, constant: 10).isActive = true
        picker01.trailingAnchor.constraint(equalTo: marcoPicker01.trailingAnchor, constant: -10).isActive = true
        
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker01,
                attribute: .top,
                relatedBy: .equal,
                toItem: picker01,
                attribute: .top,
                multiplier: 1.0,
                constant: 0))
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker01,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: picker01,
                attribute: .bottom,
                multiplier: 1.0,
                constant: 0))
        
        self.marcoPicker03.addSubview(picker02)
        picker02.translatesAutoresizingMaskIntoConstraints = false
        picker02.leadingAnchor.constraint(equalTo: marcoPicker03.leadingAnchor, constant: 10).isActive = true
        picker02.trailingAnchor.constraint(equalTo: marcoPicker03.trailingAnchor, constant: -10).isActive = true
        picker02.topAnchor.constraint(equalTo: marcoPicker03.topAnchor, constant: 25).isActive = true
        picker02.bottomAnchor.constraint(equalTo: marcoPicker03.bottomAnchor, constant: -10).isActive = true
        picker02.isHidden = true
        
        self.marcoPicker03.addSubview(picker03)
        picker03.translatesAutoresizingMaskIntoConstraints = false
        picker03.leadingAnchor.constraint(equalTo: marcoPicker03.leadingAnchor, constant: 10).isActive = true
        picker03.trailingAnchor.constraint(equalTo: marcoPicker03.trailingAnchor, constant: -10).isActive = true
        picker03.topAnchor.constraint(equalTo: marcoPicker03.topAnchor, constant: 25).isActive = true
        picker03.bottomAnchor.constraint(equalTo: marcoPicker03.bottomAnchor, constant: -10).isActive = true
        picker03.isHidden = true
        
        
        for vistas in contenedorView.subviews {
            if (vistas is UIView) {
        }
        vistas.layer.borderColor = colorAzul.cgColor
            vistas.layer.borderWidth = 3}
        //init toolbar
       
        let barraTeclado:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let hechoBoton: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.botonBarraTeclado))
        barraTeclado.setItems([flexSpace, hechoBoton], animated: false)
        barraTeclado.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.nombreTF.inputAccessoryView = barraTeclado
        self.cantidadTF.inputAccessoryView = barraTeclado
        self.volumenFarmTF.inputAccessoryView = barraTeclado
        self.volumenDisolTF.inputAccessoryView = barraTeclado
        self.dosisRecoTV.inputAccessoryView = barraTeclado


        unidadMedidaTF.inputView = picker02
        unidadDosificacionTF.inputView = picker03

        barra02 = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.marcoPicker03.frame.size.width, height: 30))
        let hechoBoton02: UIBarButtonItem = UIBarButtonItem(title: "Hecho", style: .done, target: self, action: #selector(self.botonBarra02))

        barra02.setItems([flexSpace, hechoBoton02], animated: false)
        //barra02.sizeToFit()
        //self.nombreTF.inputAccessoryView = barra02

        self.marcoPicker03.addSubview(barra02)
        
        dosisRecoTV.layer.borderWidth = 0.5
        
        dosisRecoTV.layer.borderColor = UIColor.lightGray.cgColor
        definirValores()

    }
    @objc func botonBarraTeclado() {
        marcoBotones.isUserInteractionEnabled = true
        marcoBotones.alpha = 1
        self.view.endEditing(true)
    }
    
    @objc func botonBarra02() {
        picker02.isHidden = true
        picker03.isHidden = true
        marcoPicker03.isHidden = true
        marcoBotones.isHidden = false
        for vistas in marcoBotones.subviews {
           vistas.isHidden = false
        marcoBotones.bringSubviewToFront(vistas)
        }
        nuevoFarmacoButton.isHidden = false
        
        //self.view.addSubview(marcoBotones)
       marcoBotones.isUserInteractionEnabled = true
       marcoBotones.alpha = 1
       // self.view.endEditing(true)
    }
    
    
    @IBAction func pickerCierreButton(_ sender: Any) {
                marcoBotones.addSubview(nuevoFarmacoButton)
        picker02.isHidden = true
        picker03.isHidden = true
        marcoBotones.isHidden = true

    }
    
    func ordenar(lista: [Farmaco]) {
        
        let lista = lista.sorted(by: {$0.nombre < $1.nombre})
        let farmacosIn = try! JSONEncoder().encode(lista)
        UserDefaults.standard.set(farmacosIn, forKey: "FarmacosDefault")
        unidadPrimeraVez = true
        picker01.reloadAllComponents()
        
        
        for i in (0...lista.count-1) {
            print("Farmaco: \(lista[i].nombre), \(lista[i].picker03Indice01), \(lista[i].picker03Indice02), \(lista[i].picker03Indice03)")
        }
        
       
        posicionPicker = 0
        for valores in 0...lista.count - 1 {
            if (lista[valores].nombre) == nombreTF.text {
                posicionPicker = valores}
        }
        UserDefaults.standard.set(posicionPicker, forKey: "posicionFarmacoDefault")
        definirValores()
    }

    
    @IBAction func eliminarFarmaco(_ sender: Any) {
        accionRealizar = 1
        self.mostrarAnimacion(vista: marcoAvisos)
        marcoAvisos.isHidden = false
        marcoPicker01.isUserInteractionEnabled = false
        marcoBotones.isUserInteractionEnabled = false
        marcoValores.isUserInteractionEnabled = false
        if listaFarmacos.count == 1{
            avisosLabel.text = "No se puede eliminar el último Fármaco"
            aceptarAvisosButton.isHidden = true
        }
        else {
        avisosLabel.text = "¿Estás seguro de que quieres eliminar el fármaco elegido?"
        }
    }
    @IBAction func reponerFarmacosBasales(_ sender: Any) {
        accionRealizar = 2
        self.mostrarAnimacion(vista: marcoAvisos)
        marcoAvisos.isHidden = false
        marcoPicker01.isUserInteractionEnabled = false
        marcoBotones.isUserInteractionEnabled = false
        marcoValores.isUserInteractionEnabled = false
        avisosLabel.text = "¿Estás seguro de que quieres reponer los fármacos iniciales?\nSe borrarán todos los fármacos introducidos manualmente"
    }
    @IBAction func modificarFarmaco(_ sender: Any) {
        accionRealizar = 3
        self.mostrarAnimacion(vista: marcoAvisos)
        marcoAvisos.isHidden = false
        marcoPicker01.isUserInteractionEnabled = false
        marcoBotones.isUserInteractionEnabled = false
        marcoValores.isUserInteractionEnabled = false
        avisosLabel.text = "¿Estás seguro de que quieres modificar el Fármaco elegido?"
        var datosValidos = 0
        avisosLabel.text = ""
        if comprobarNumero(comprobar: cantidadTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Cantidad de Fármaco no es correcto"
            datosValidos = 1 }
        if unidadMedidaTF.text == "" {
            avisosLabel.text = avisosLabel.text! + "\nLa Unidad del Fármaco no puede ser nula"
            datosValidos = 1 }
        if comprobarNumero(comprobar: volumenFarmTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Volumen de Fármaco no es correcto"
            datosValidos = 1 }
        if comprobarNumero(comprobar: volumenDisolTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Volumen de Disolvente no es correcto"
            datosValidos = 1 }
        if dosisRecoTV.text == nil{
            dosisRecoTV.text = ""}
        
        if datosValidos == 1 {
            aceptarAvisosButton.isHidden = true
        }
        //print("Datos válidos \(datosValidos)")
        
        if datosValidos == 0 {
            aceptarAvisosButton.isHidden = false
            avisosLabel.text = "¿Estás seguro de que quieres modificar el Fármaco?"
        }
    }
    @IBAction func guardarFarmaco(_ sender: Any) {
        accionRealizar = 4
        self.mostrarAnimacion(vista: marcoAvisos)

        marcoAvisos.isHidden = false
        marcoPicker01.isUserInteractionEnabled = false
        marcoBotones.isUserInteractionEnabled = false
        marcoValores.isUserInteractionEnabled = false
        var datosValidos = 0
        avisosLabel.text = ""
        let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
        var lista = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
        for valores in 0...lista.count - 1 {
            if lista[valores].nombre == nombreTF.text {
                avisosLabel.text = "El nombre ya existe, no se puede volver a utilizar"
                datosValidos = 1}
            }
        if comprobarNumero(comprobar: cantidadTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Cantidad de Fármaco no es correcto"
            datosValidos = 1 }
        if unidadMedidaTF.text == "" {
            avisosLabel.text = avisosLabel.text! + "\nLa Unidad del Fármaco no puede ser nula"
            datosValidos = 1 }
        
        if comprobarNumero(comprobar: volumenFarmTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Volumen de Fármaco no es correcto"
            datosValidos = 1 }
        if comprobarNumero(comprobar: volumenDisolTF) == 1{
            avisosLabel.text = avisosLabel.text! + "\nEl valor de Volumen de Disolvente no es correcto"
            datosValidos = 1 }
        if dosisRecoTV.text == nil{
            dosisRecoTV.text = ""}
        
        if datosValidos == 1 {
            aceptarAvisosButton.isHidden = true
        }
        //print("Datos válidos \(datosValidos)")
        
        if datosValidos == 0 {
            aceptarAvisosButton.isHidden = false
            avisosLabel.text = "¿Estás seguro de que quieres guardar el nuevo Fármaco?"
        }
    }

    func comprobarNumero(comprobar: UITextField)->Int{
        
        let permitidos = comprobar.text!
        //print("Texto enviado: \(comprobar.text)")
        //print("Texto procesado: \(permitidos)")
        
        if (CharacterSet(charactersIn: "0123456789.").isSuperset(of: CharacterSet(charactersIn: permitidos)) && permitidos != ""){
            return 0
        }

        else{
            return 1}
    }
        
    @IBAction func salirAcciones(_ sender: Any) {
        accionRealizar = 0
        marcoAvisos.alpha = 1
        aceptarAvisosButton.isHidden = false
        marcoAvisos.isHidden = true
        avisosLabel.text = ""
        marcoPicker01.isUserInteractionEnabled = true
        marcoBotones.isUserInteractionEnabled = true
        marcoValores.isUserInteractionEnabled = true

        definirValores()
    }
    
    @IBAction func aceptarAcciones(_ sender: Any) {
        aceptarAvisosButton.isHidden = true
        let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
        var lista = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
        
        if accionRealizar == 1 {
            // Eliminar Fármaco

            lista.remove(at: posicion)
            avisosLabel.text = "El Fármaco ha sido eliminado"
            }
        if accionRealizar == 2{
            // Reponer Fármacos basales
            let Noradrenalina = Farmaco(nombre: "Noradrenalina", cantidad: 20, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 100, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Dobutamina = Farmaco(nombre: "Dobutamina", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Dopamina = Farmaco(nombre: "Dopamina", cantidad: 200, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 45, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Adrenalina = Farmaco(nombre: "Adrenalina", cantidad: 5, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 100, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Urapidilo = Farmaco(nombre: "Urapidilo", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 250, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Levosimendan = Farmaco(nombre: "Levosimendan", cantidad: 12.5, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 500, dosisRecomendada: "0.05 - 0.2 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Labetalol = Farmaco(nombre: "Labetalol", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "2 - 10 mg/min", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 1)
            let Nitroglicerina = Farmaco(nombre: "Nitroglicerina", cantidad: 10, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 0, picker03Indice03: 1)
            let Nimodipino = Farmaco(nombre: "Nimodipino", cantidad: 10, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "2 mg/hora", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
            let Propofol = Farmaco(nombre: "Propofol", cantidad: 1000, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "4 - 8 mg/kg/hora", picker03Indice01: 2, picker03Indice02: 1, picker03Indice03: 2)
            let Midazolam = Farmaco(nombre: "Midazolam", cantidad: 100, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "0.03 - 0.2 mg/kg/hora", picker03Indice01: 2, picker03Indice02: 1, picker03Indice03: 2)
            let Dexmedetomidina = Farmaco(nombre: "Dexmedetomidina", cantidad: 20, picker02Indice01: 1, volumenFarmaco: 2, volumenDisolucion: 48, dosisRecomendada: "0.7 - 1.4 mcg/kg/hora", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 2)
            let Remifentanilo = Farmaco(nombre: "Remifentanilo", cantidad: 2, picker02Indice01: 2, volumenFarmaco: 2, volumenDisolucion: 48, dosisRecomendada: "0.5 - 1.0 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let CloruroMorfico = Farmaco(nombre: "Cloruro Mórfico", cantidad: 50, picker02Indice01: 2, volumenFarmaco: 3, volumenDisolucion: 47, dosisRecomendada: "0.8 - 10 mg/hora", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
            let Cisatracurio = Farmaco(nombre: "Cisatracurio", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Fentanilo = Farmaco(nombre: "Fentanilo", cantidad: 750, picker02Indice01: 1, volumenFarmaco: 3, volumenDisolucion: 47, dosisRecomendada: "1 - 5 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
            let Furosemida = Farmaco(nombre: "Furosemida", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 25, volumenDisolucion: 25, dosisRecomendada: "", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
            
        lista = [Noradrenalina, Dobutamina, Dopamina, Adrenalina, Urapidilo, Levosimendan, Labetalol, Nitroglicerina, Nimodipino, Propofol, Midazolam, Dexmedetomidina, Remifentanilo, CloruroMorfico, Cisatracurio, Fentanilo, Furosemida]
            avisosLabel.text = "Se han repuesto los Fármacos basales"
            }
        if accionRealizar == 3{
            // Modificar Fármaco
            
            let unidad = UserDefaults.standard.integer(forKey: "unidadMedidaFarmacoDefault")
            let indice2_0 = unidad

            let array = UserDefaults.standard.array(forKey: "unidadIngresoDefault") ?? [] as! [Int]
           // print("Unidad defalult antes de grabar: \(array)")
            let indice3_0 = array[0] as! Int
            let indice3_1 = array[1] as! Int
            let indice3_2 = array[2] as! Int
            
        
            
            lista[posicion].nombre = nombreTF.text!
            lista[posicion].cantidad = Double(cantidadTF.text!)!
            lista[posicion].picker02Indice01 = indice2_0
            lista[posicion].volumenFarmaco = Double(volumenFarmTF.text!)!
            lista[posicion].volumenDisolucion = Double(volumenDisolTF.text!)!
            lista[posicion].dosisRecomendada = dosisRecoTV.text!
            lista[posicion].picker03Indice01 = indice3_0
            lista[posicion].picker03Indice02 = indice3_1
            lista[posicion].picker03Indice03 = indice3_2
            
            avisosLabel.text = "El Fármaco ha sido modificado"
            
        }
        if accionRealizar == 4 {
            // Nuevo Fármaco
            let unidad = UserDefaults.standard.integer(forKey: "unidadMedidaFarmacoDefault")
            let indice2_0 = unidad
            
            let array = UserDefaults.standard.array(forKey: "unidadIngresoDefault") ?? [] as! [Int]
            let indice3_0 = array[0] as! Int
            let indice3_1 = array[1] as! Int
            let indice3_2 = array[2] as! Int
            
            let nuevoFarmaco = Farmaco(nombre: nombreTF.text!, cantidad: Double(cantidadTF.text!)!, picker02Indice01: indice2_0, volumenFarmaco: Double(volumenFarmTF.text!)!, volumenDisolucion: Double(volumenDisolTF.text!)!, dosisRecomendada: dosisRecoTV.text!, picker03Indice01: indice3_0, picker03Indice02: indice3_1, picker03Indice03: indice3_2)
            lista.append(nuevoFarmaco)
            avisosLabel.text = "Se ha añadido el nuevo Fármaco"
           // ordenar(lista: lista)
            
        }
        marcoAvisos.isHidden = false
        marcoPicker01.isUserInteractionEnabled = false
        marcoBotones.isUserInteractionEnabled = false
        marcoValores.isUserInteractionEnabled = false

        ordenar(lista: lista)
        
    }
    @IBAction func cerrarPopUp(_ sender: Any) {
        let indiceFarmaco = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
        
        let nuevaUnidad = UserDefaults.standard.array(forKey: "unidadIngresoDefault")
        UserDefaults.standard.set(nuevaUnidad, forKey: "unidadDefault")

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RecargarVista"), object: nil)
        ocultarAnimacion(vista: view)
}
    
    
    func mostrarAnimacion(vista: UIView){
        vista.backgroundColor = colorGris
        vista.alpha = 0.8
        vista.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        vista.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, animations: {
            vista.transform = CGAffineTransform(scaleX: 1, y: 1)
            vista.alpha = 1
        })
    }
    func ocultarAnimacion(vista: UIView){
        
        UIView.animate(withDuration: 0.6, delay: 0, animations: {
            vista.transform = CGAffineTransform(scaleX: 1, y: 1)
            vista.alpha = 0
        }, completion: {(finished: Bool) in
            if (finished){
                vista.removeFromSuperview()}
        })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case picker01:
            return 1
        case picker02:
            return 1
        default:
            return 3
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case picker01:
            let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
            listaFarmacos = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
            return listaFarmacos.count
        case picker02:
            return unidad.count
        default:
            if component == 0 {
                return unidad.count}
            if component == 1 {
                return unidadReferencia.count}
            else{
                return tiempo.count}
        }
}
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case picker01:
            posicion = row
            return listaFarmacos[row].nombre
        case picker02:
            //posicion = row
            return unidad[row]
        default:
            if component == 0 {
                return unidad[row]}
            else if component == 1{
                return unidadReferencia[row]}
            return tiempo[row]
        }
        }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let w = pickerView.frame.size.width
        switch pickerView{
        case picker03:
            switch component{
            case 0: return w * 1.6/5
            case 1: return w * 1/5
            default: return w * 2.4/5}
        case picker01:
            return w
        default:
            return w/4
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // Establecer valore default para los pickerviews
        //Picker01. Si es primera vez que se elige el fármaco se usan los valores basales. Con el if se situa como primera vez el farmaco cada vez que se modifica el picker01
        
        switch pickerView {
            case picker01:
                unidadPrimeraVez = true
                let picker01Indice0 = picker01.selectedRow(inComponent: 0)
                UserDefaults.standard.set(picker01Indice0, forKey: "posicionFarmacoDefault")
            case picker02:
                let picker02Indice0 = picker02.selectedRow(inComponent: 0)
                UserDefaults.standard.set(picker02Indice0, forKey: "unidadMedidaFarmacoDefault")
            default:
                //Picker03
                let picker03Indice0 = picker03.selectedRow(inComponent: 0)
                let picker03Indice1 = picker03.selectedRow(inComponent: 1)
                let picker03Indice2 = picker03.selectedRow(inComponent: 2)
                let picker03IndiceDefault = [picker03Indice0, picker03Indice1, picker03Indice2]
                UserDefaults.standard.set(picker03IndiceDefault, forKey: "unidadIngresoDefault")
            }
        definirValores()
    }
    
    func definirValores(){
            // Si es la primera vez que se entra en ese fármaco a través del picker01, se guardan los datos almacenados en FarmacosDefault
        switch unidadPrimeraVez {
            case true:
                let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
                listaFarmacos = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
                let indiceFarmaco = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
                
                nombreTF.text = listaFarmacos[indiceFarmaco].nombre
                cantidadTF.text = String(format: "%.0f",listaFarmacos[indiceFarmaco].cantidad)
                volumenFarmTF.text = String(format: "%.0f",listaFarmacos[indiceFarmaco].volumenFarmaco)
                volumenDisolTF.text = String(format: "%.0f",listaFarmacos[indiceFarmaco].volumenDisolucion)
                dosisRecoTV.text = listaFarmacos[indiceFarmaco].dosisRecomendada
                
                let picker02Indice0 = listaFarmacos[indiceFarmaco].picker02Indice01
                unidadMedidaTF.text = unidad[picker02Indice0]
                /*
                switch picker02Indice0 {
                    case 0:
                        unidadMedidaTF.text = ""
                    case 1:
                        unidadMedidaTF.text = "mcg"
                    case 2:
                        unidadMedidaTF.text = "mg"
                    case 3:
                        unidadMedidaTF.text = "g"
                    default:
                        break
                    }
                */
                
                let picker03Indice0 = listaFarmacos[indiceFarmaco].picker03Indice01
                let picker03Indice1 = listaFarmacos[indiceFarmaco].picker03Indice02
                let picker03Indice2 = listaFarmacos[indiceFarmaco].picker03Indice03
                unidadDosificacionTF.text = obtenerUnidad(indice_0: picker03Indice0, indice_1: picker03Indice1, indice_2: picker03Indice2)
                let picker03IndiceDefault = [picker03Indice0, picker03Indice1, picker03Indice2]
                
                UserDefaults.standard.set(picker03IndiceDefault, forKey: "unidadIngresoDefault")
                
                
                
                unidadPrimeraVez = false

            default:
                let picker02Indice0 = UserDefaults.standard.integer(forKey: "unidadMedidaFarmacoDefault")
                unidadMedidaTF.text = unidad[picker02Indice0]
            
                let indicePicker03 = UserDefaults.standard.array(forKey: "unidadIngresoDefault") as! [Int]
                let picker03Indice0 = indicePicker03[0]
                let picker03Indice1 = indicePicker03[1]
                let picker03Indice2 = indicePicker03[2]
                
                let picker03IndiceDefault = [picker03Indice0, picker03Indice1, picker03Indice2]
                
                UserDefaults.standard.set(picker03IndiceDefault, forKey: "unidadIngresoDefault")
                unidadDosificacionTF.text = obtenerUnidad(indice_0: picker03Indice0, indice_1: picker03Indice1, indice_2: picker03Indice2)
            
        }
        ocultarRecomendacion()
        situarPicker()

    
    }
    
    
    
    func obtenerUnidad(indice_0: Int, indice_1:Int, indice_2: Int) -> (String){
        var texto: String = ""
        
        /*
        let array = UserDefaults.standard.array(forKey: "unidadIngresoDefault") ?? [0,0,0]
        
        let indice_0 = array[0] as! Int
        let indice_1 = array[1] as! Int
        let indice_2 = array[2] as! Int
        */
        
        let valor0 = unidad[indice_0]
        let valor1 = unidadReferencia[indice_1]
        let valor2 = tiempo[indice_2]
        
        switch (indice_0, indice_1, indice_2) {
        case (1..., 1, 1...):
            texto = "\(valor0)/\(valor1)/\(valor2)"
            unidadValida = true
        case (1..., 0, 1...):
            texto = "\(valor0)/\(valor2)"
            unidadValida = true
        default:
            texto = ""
            unidadValida = false
        }
        return texto
}
    
    func situarPicker() {
        //picker01
        let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
        let lista = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
        let indice01_0 = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
        picker01.selectRow(indice01_0, inComponent: 0, animated: true)
        //print("indice01_0: \(indice01_0)")
        
        //picker02
        let indice02_0 = UserDefaults.standard.integer(forKey: "unidadMedidaFarmacoDefault")
        picker02.selectRow(indice02_0, inComponent: 0, animated: true)
        
        //picker03
        let lista03 = UserDefaults.standard.array(forKey: "unidadIngresoDefault") as! [Int]
        picker03.selectRow(lista03[0], inComponent: 0, animated: true)
        picker03.selectRow(lista03[1], inComponent: 1, animated: true)
        picker03.selectRow(lista03[2], inComponent: 2, animated: true)
    }

    func ocultarRecomendacion(){
        let texto = dosisRecoTV.text
        if texto == "" || texto == "Dosis recomendada" {
            dosisRecoTV.text = "Dosis recomendada"
            dosisRecoTV.font = UIFont.italicSystemFont(ofSize: 20)
            dosisRecoTV.textColor = .lightGray }
        else {
            dosisRecoTV.font = UIFont.systemFont(ofSize: 20)
            dosisRecoTV.textColor = .black
            dosisRecoTV.text = texto
        }
    }
    

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        marcoBotones.isUserInteractionEnabled = true
        marcoBotones.alpha = 1
        ocultarRecomendacion()
        
        for vistas in self.marcoValores.subviews{
            if(vistas.isFirstResponder) {
                vistas.endEditing(true)
                vistas.resignFirstResponder()
                //print("Return false")
                return false
                }
        }
        textField.endEditing(true)
        //print("return true")
        return true
    }
   
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        marcoBotones.isUserInteractionEnabled = true
        marcoBotones.alpha = 1
        dosisRecoTF.isHidden = false
        activoTextField = textField

        for vista in self.marcoValores.subviews{
                vista.layer.borderWidth = 1
                vista.layer.borderColor = UIColor.clear.cgColor
                // vista.isUserInteractionEnabled = false
                // vista.resignFirstResponder()
        }
        activoTextField.layer.borderWidth = 1
        activoTextField.layer.borderColor = UIColor.blue.cgColor

        if activoTextField == unidadMedidaTF {
            marcoPicker03.isHidden = false
            picker01.isHidden = false
            picker02.isHidden = false
            picker03.isHidden = true
            marcoBotones.isHidden = true

            activoTextField.endEditing(true)
            activoTextField.resignFirstResponder()
            marcoBotones.isHidden = false
        
        
    }
 
    else if activoTextField == unidadDosificacionTF{
        marcoPicker03.isHidden = false
        picker01.isHidden = false
        picker02.isHidden = true
        picker03.isHidden = false
        marcoBotones.isHidden = true
        activoTextField.resignFirstResponder()
        unidadDosificacionTF.endEditing(true)
        unidadDosificacionTF.resignFirstResponder()
        marcoBotones.isHidden = false
    

    }
   
    else if activoTextField == dosisRecoTF{
        marcoPicker03.isHidden = true
        picker01.isHidden = false
        picker02.isHidden = true
        picker03.isHidden = true
        marcoBotones.isUserInteractionEnabled = false
            
        marcoBotones.alpha = 0.5
        dosisRecoTF.isHidden = true
        dosisRecoTV.layer.borderWidth = 1
        dosisRecoTV.layer.borderColor = UIColor.blue.cgColor
        dosisRecoTV.becomeFirstResponder()
        dosisRecoTV.endEditing(false)
        
            
        if dosisRecoTV.text == "Dosis recomendada" {
            dosisRecoTV.text = ""
            dosisRecoTV.font = UIFont.systemFont(ofSize: 20)
            dosisRecoTV.textColor = .black
            }
        
        //activoTextField.endEditing(true)
        }
        
        
        else if (activoTextField != unidadMedidaTF && activoTextField != unidadDosificacionTF && activoTextField != dosisRecoTF){
        marcoPicker03.isHidden = true
        picker01.isHidden = false
        picker02.isHidden = true
        picker03.isHidden = true
        marcoBotones.isUserInteractionEnabled = false
        marcoBotones.alpha = 0.5
        //activoTextField.endEditing(true)
    }
        // definirValores()
    }
    /*
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        for vistas in self.marcoValores.subviews{
            //if(vistas.isFirstResponder) {
                vistas.endEditing(true)
                vistas.resignFirstResponder()
                //print("Return false")
           // }
            }
    }
    */
    
}

