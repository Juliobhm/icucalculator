//
//  pesiController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 19/11/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class pesiController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    
    @IBOutlet var barraSuperior: UIToolbar!
    @IBOutlet var sumaPesi: UILabel!
    @IBOutlet var edadLabel: UILabel!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var barraPicker: UIToolbar!
    @IBOutlet var pesiReducidoColection: [UIStackView]!
    @IBOutlet var switchColection: [UISwitch]!
    @IBOutlet var degradadoView: UIView!
    @IBOutlet var edadTextField: UITextField!
    @IBOutlet var resultadosView: UIView!
    @IBOutlet var mortalidadLabel: UILabel!
    let gradiente = CAGradientLayer()
    let edad = ["","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"]

    var marcadorTipoPesi: Int = 1
    var suma: Int = 0
    var riesgo: String!
    var mortalidad: String!
    var colorAzul: UIColor = UIColor(red: 0/255, green: 74/255, blue: 211/255, alpha: 1.0)
    var colorFondoPicker: UIColor = UIColor(red: 173/255, green: 218/255, blue: 247/255, alpha: 1.0)
   
    

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return edad.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return edad[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     
       
        switch marcadorTipoPesi{
           case 1:
                if edadTextField.text != "" {
                    suma = suma - Int(edadTextField.text!)!}
                    edadTextField.text = edad[row]
                if edad[row] != ""{
                    suma = suma + Int(edad[row])!}
  
            case 0:
                if (edadTextField.text != ""  && Int(edadTextField.text!)! > 80){
                    suma = suma - 1}
                
                    edadTextField.text = edad[row]
                if (edad[row] != "" && Int(edad[row])! > 80){
                    suma = suma + 1}
            default:
                break
        }
        mostrarSuma(sumaTotal: suma)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.selectRow(71, inComponent: 0, animated: false)
        if marcadorTipoPesi == 0 {
           
            for valores in pesiReducidoColection {
                valores.alpha = 0.2
                valores.isUserInteractionEnabled = false
            }
        }
        sumaPesi.text = "Puntos"
        riesgo = ""
        view.layer.borderWidth = 2
        view.layer.borderColor = colorAzul.cgColor
        
        edadTextField.text = ""
        edadTextField.layer.borderColor = colorAzul.cgColor
        edadTextField.layer.borderWidth = 2
        edadTextField.layer.cornerRadius = 15
        resultadosView.layer.borderColor = colorAzul.cgColor
        resultadosView.layer.borderWidth = 2
        resultadosView.backgroundColor = colorFondoPicker
        barraSuperior.layer.borderWidth = 2
        barraSuperior.layer.borderColor = colorAzul.cgColor
        

        
        
        for valores in switchColection {
            valores.tintColor = colorAzul
            valores.onTintColor = colorAzul
        }
            
        crearPickerView()

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        pickerView.isHidden = false
      
        barraPicker.isHidden = false
        gradiente.frame = degradadoView.bounds
        gradiente.colors = [UIColor.black.cgColor, UIColor.clear.cgColor, UIColor.clear.cgColor]
        gradiente.locations = [0, 0.6, 1]
        degradadoView.layer.mask = gradiente
        edadTextField.isUserInteractionEnabled = false
        edadTextField.resignFirstResponder()
    }
    
    @IBAction func hechoPickerButton(_ sender: Any) {
        pickerView.isHidden = true
        barraPicker.isHidden = true
        degradadoView.layer.mask = nil
        edadTextField.isUserInteractionEnabled = true
        //edadTextField.resignFirstResponder()
    }

    func crearPickerView(){
        barraPicker.barStyle = .blackTranslucent
        barraPicker.tintColor = .white
        barraPicker.backgroundColor = colorAzul
        pickerView.layer.borderWidth = 2
        pickerView.layer.borderColor = colorAzul.cgColor
        pickerView.backgroundColor = colorFondoPicker
        
    }
 
    @IBAction func resetButton(_ sender: Any) {
        
        for valores in switchColection {
            valores.isOn = false
          
        }
        edadTextField.text = ""
        suma = 0
        sumaPesi.text = "Total"
        mortalidadLabel.text = "Mortalidad"
    }
    
    
    @IBAction func switchSexo(_ sender: UISwitch) {
        switch marcadorTipoPesi {
            case 1 :
                if (sender.isOn == true){
                    suma = suma + 10
                    }
                else if (sender.isOn == false) {suma = suma - 10
                }
            case 0:
                    if (sender.isOn == true){
                        suma = suma + 1
                    }
                    else if (sender.isOn == false) {suma = suma - 1
                    }
            default:
                break
        }
        mostrarSuma(sumaTotal: suma)
    }
    
    @IBAction func switchCancer(_ sender: UISwitch) {
        switch marcadorTipoPesi {
        case 1:
            if (sender.isOn == true){
            suma = suma + 30
        }
        else if (sender.isOn == false) {suma = suma - 30
        }
        
        case 0:
            if (sender.isOn == true){
                suma = suma + 1
            }
            else if (sender.isOn == false) {suma = suma - 1
            }
        default:
            break
  
    }
 mostrarSuma(sumaTotal: suma)
    }
        
    @IBAction func switchInsufCardiaca(_ sender: UISwitch) {
        switch marcadorTipoPesi{
        case 1:
            if (sender.isOn == true){
                suma = suma + 10
            }
            else if (sender.isOn == false) {suma = suma - 10
            }
        case 0:
            if (sender.isOn == true){
                suma = suma + 1
            }
            else if (sender.isOn == false) {suma = suma - 1
            }
        default:
            break
    }
        mostrarSuma(sumaTotal: suma)
    }
    
   
    
    
    @IBAction func switchEpoc(_ sender: UISwitch) {
        if (sender.isOn == true){
            suma = suma + 10
        }
        else if (sender.isOn == false) {suma = suma - 10
        }
        mostrarSuma(sumaTotal: suma)
    }
    
    @IBAction func switcFreCardiaca(_ sender: UISwitch) {
        switch marcadorTipoPesi{
            case 1:
                if (sender.isOn == true){
                    suma = suma + 20
                }
                else if (sender.isOn == false) {suma = suma - 20
                }
                mostrarSuma(sumaTotal: suma)
        case 0 :
                if (sender.isOn == true){
                    suma = suma + 1
                }
                else if (sender.isOn == false) {suma = suma - 1
                }
            
        default:
                break
        }
        mostrarSuma(sumaTotal: suma)
    }

    @IBAction func switcTension(_ sender: UISwitch) {
        switch marcadorTipoPesi{
            case 1:
                if (sender.isOn == true){
                suma = suma + 30
            }
                else if (sender.isOn == false) {suma = suma - 30
            }
           
            case 0:
                if (sender.isOn == true){
                    suma = suma + 1
                }
                else if (sender.isOn == false) {suma = suma - 1
                }
            default:
                break
        }
        mostrarSuma(sumaTotal: suma)
    }
    
    @IBAction func switcFreReapiratoria(_ sender: UISwitch) {
        if (sender.isOn == true){
            suma = suma + 20
        }
        else if (sender.isOn == false) {suma = suma - 20
        }
        mostrarSuma(sumaTotal: suma)
    }
    
    @IBAction func switcTemperatura(_ sender: UISwitch) {
        if (sender.isOn == true){
            suma = suma + 20
        }
        else if (sender.isOn == false) {suma = suma - 20
        }
        mostrarSuma(sumaTotal: suma)
    }
    @IBAction func switcEstadoMental(_ sender: UISwitch) {
        if (sender.isOn == true){
            suma = suma + 60
        }
        else if (sender.isOn == false) {suma = suma - 60
        }
        mostrarSuma(sumaTotal: suma)
    }
    @IBAction func switcSatOxigeno(_ sender: UISwitch) {
        switch marcadorTipoPesi{
            case 1:
                    if (sender.isOn == true){
                        suma = suma + 20
                    }
                    else if (sender.isOn == false) {suma = suma - 20
                    }
            case 0:
                if (sender.isOn == true){
                    suma = suma + 1
                }
                else if (sender.isOn == false) {suma = suma - 1
                }
            default:
                break
            }
        mostrarSuma(sumaTotal: suma)
    }
    
    
    func mostrarSuma(sumaTotal: Int){
        switch marcadorTipoPesi{
            case 1:
                if suma <= 65{
                    riesgo = "Clase I (riesgo muy bajo) <65 puntos"
                    mortalidad = "1,1% (0,7 - 1,7)"
                }
                else if (suma >= 66  && suma <= 85){
                    riesgo = "Clase II (riesgo bajo) 66-85 puntos"
                    mortalidad = "3,1% (2,5 - 4,0)"
                }
                else if (suma >= 86 && suma <= 105){
                    riesgo = "Clase III (riesgo intermedio) 86-105 puntos"
                    mortalidad = "6,5% (5,5 - 7,6)"
                }
                else if (suma >= 106 && suma <= 125) {
                    riesgo = "Clase IV (riesgo alto) 106-125 puntos"
                    mortalidad = "10,4% (9,0 - 11,9)"
                }
                else if suma >= 126 {
                    riesgo = "Clase V (riesgo muy alto) >125 puntos"
                    mortalidad = "24,5% (22,7 - 26,4)"
                }
                sumaPesi.text = ("Puntos: \(String(sumaTotal))\n \(riesgo ?? "0")")
                mortalidadLabel.text = "Mortalidad a los 30 días:\n \(mortalidad ?? "0%")"

            case 0:
                if suma == 0{
                    riesgo = "Riesgo bajo: 0 puntos"
                    mortalidad = "1% (0,0 - 2,1)"
                }
                else if suma >= 1{
                    riesgo = "Riesgo alto: >= 1 punto"
                    mortalidad = "10,9% (8,5 - 13,2)"
                }
            default:
                break
                }
            sumaPesi.text = ("Puntos: \(String(sumaTotal))\n \(riesgo ?? "0")")
            mortalidadLabel.text = "Mortalidad a los 30 días:\n \(mortalidad ?? "0%")"
            }
 }


