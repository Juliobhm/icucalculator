//
//  ProtocoloViewController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 27/11/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit
import WebKit

class ProtocoloViewController: UIViewController {

@IBOutlet var pdfWebView: WKWebView!
    
     var colorAzul: UIColor = UIColor(red: 0/255, green: 74/255, blue: 211/255, alpha: 1.0)
    override func viewDidLoad() {
        super.viewDidLoad()

let direccion = Bundle.main.path(forResource: "Protocolo TEP", ofType: "pdf")
    
let archivo = URL(fileURLWithPath: direccion!)
let respuesta = URLRequest(url: archivo)
        pdfWebView.load(respuesta)
        
pdfWebView.layer.borderWidth = 2
pdfWebView.layer.borderColor = colorAzul.cgColor

    }
        
    }



