//
//  PerfusionesViewController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 15/12/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class PerfusionesViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UIScrollViewDelegate {
    
    class Farmaco: Codable {
        var nombre: String
        var cantidad: Double
        var picker02Indice01: Int = 0
        var volumenFarmaco: Double
        var volumenDisolucion: Double
        var picker03Indice01: Int = 0
        var picker03Indice02: Int = 0
        var picker03Indice03: Int = 0
        var dosisRecomendada: String
        
        init(nombre : String, cantidad: Double, picker02Indice01: Int, volumenFarmaco: Double, volumenDisolucion: Double, dosisRecomendada: String, picker03Indice01: Int, picker03Indice02: Int, picker03Indice03: Int) {
            self.nombre = nombre
            self.cantidad = cantidad
            self.picker02Indice01 = picker02Indice01
            self.volumenFarmaco = volumenFarmaco
            self.volumenDisolucion = volumenDisolucion
            self.picker03Indice01 = picker03Indice01
            self.picker03Indice02 = picker03Indice02
            self.picker03Indice03 = picker03Indice03
            self.dosisRecomendada = dosisRecomendada
        }
    }
    var listaFarmacos: [Farmaco] = []
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contenedorView: UIView!
    @IBOutlet var marcoPicker01: UIView!
    @IBOutlet var marcoPicker02: UIView!
    @IBOutlet var marcoPicker03: UIView!
    @IBOutlet var pesoTextField: UITextField!
    @IBOutlet var farmacoTextField: UITextField!
    @IBOutlet var cantidadFarmaco: UITextField!
    @IBOutlet var volumenDilucion: UITextField!
    @IBOutlet var unidadesTextField: UITextField!
    @IBOutlet var dosisTextField: UITextField!
    @IBOutlet var mlHoraTextField: UITextField!
    
    @IBAction func tocarDosis(_ sender: Any) {
        dosisVelocidad = 1
        marcadorCampo = "Dosis"
        definirValores()
    }
    @IBAction func tocarVelocidad(_ sender: Any) {
        dosisVelocidad = 0
        marcadorCampo = "mL/h"
        definirValores()
    }
    
    @IBAction func tocarPeso(_ sender: Any) {
        marcadorCampo = "Peso"
        definirValores()
    }
    
    var farmaco = ["Cisatracurio", "Dexmedetomidina", "Dobutamina", "Dopamina", "Fentanilo", "Midazolam", "Mórfico", "Noradrenalina", "Propofol"]
    var unidad = ["","mcg", "mg", "g"]
    var unidadReferencia = ["", "kg"]
    var tiempo = ["","minuto", "hora", "día"]
    var pesoEntero = [""]
    var pesoDecimal = [""]
    var entero: String = ""

    var marcadorCampo: String = "Peso"
    
    var pesoValue: Double = 0.0
    var cantidadValue: Double = 0.0
    var unidadValue: String = "mg"
    var cantidadCalculos: Double = 0.0
    var volumenValue: Double = 0.0
    var dosisValue: Double = 0.0
    var velocidadValue: Double = 0.0
    var dosisVelocidad: Int = 0
    var correctorDosisVelocidad01: Double = 0
    var correctorDosisVelocidad02: Double = 0
    var numeroColumna: Int = 0
    var posicionActual: [Int]!
    var valorNumerico: Double = 0.0
    var unidadValida: Bool = false
    var cargarDatosDeNuevo: Bool = true
    var unidadPrimeraVez: Bool = true
    
    var picker01 = UIPickerView()
    var picker02 = UIPickerView()
    var picker03 = UIPickerView()
    var label01 = UILabel()
    var colorAzul: UIColor = UIColor(red: 0/255, green: 74/255, blue: 211/255, alpha: 1.0)
    var colorAzulClaro = UIColor(red: 172/255, green: 212/255, blue: 249/255, alpha: 1.0)
    var colorGrisClaro = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView{
        case picker01:
            return 1
        case picker02:
            return 4
        default:
            return 3
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView{
        case picker01:
            let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
            listaFarmacos = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
            return listaFarmacos.count
        case picker02:
            if component == 0 {
                return pesoEntero.count}
            else {
                return pesoDecimal.count}
        default:
            if component == 0 {
                return unidad.count}
            if component == 1 {
                return unidadReferencia.count}
            else{
                return tiempo.count}
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView{
        case picker01:
            numeroColumna = row
            return listaFarmacos[row].nombre
        
        case picker02:
            if component == 0 {
                return pesoEntero[row] }
            else {
                return pesoDecimal[row]}
        default:
            if component == 0 {
                return unidad[row]}
            else if component == 1{
                return unidadReferencia[row]}
            return tiempo[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let w = pickerView.frame.size.width
        switch pickerView{
            case picker03:
                switch component{
                    case 0: return w * 1.6/5
                    case 1: return w * 1/5
                    default: return w * 2.4/5}
            case picker01:
                return w
            default:
                return w/4
        }
        
        }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // Establecer valore default para los índices de los pickerviews
        // Picker01
        
        if pickerView == picker01 {
            unidadPrimeraVez = true
        }
        
        let picker01Indice0 = picker01.selectedRow(inComponent: 0)
        UserDefaults.standard.set(picker01Indice0, forKey: "posicionFarmacoDefault")
        
        //Picker02
        let picker02Indice0 = picker02.selectedRow(inComponent: 0)
        let picker02Indice1 = picker02.selectedRow(inComponent: 1)
        let picker02Indice2 = picker02.selectedRow(inComponent: 2)
        let picker02Indice3 = picker02.selectedRow(inComponent: 3)
        posicionActual = [picker02Indice0, picker02Indice1, picker02Indice2, picker02Indice3]
        
        UserDefaults.standard.set(posicionActual, forKey: "posicionActual")
       
        switch marcadorCampo {
            case "Peso":
                UserDefaults.standard.set(posicionActual, forKey: "pesoDefault")
        case "Dosis":
                UserDefaults.standard.set(posicionActual, forKey: "dosisDefault")
            case "mL/h":
                UserDefaults.standard.set(posicionActual, forKey: "velocidadDefault")

            default:
                break
            }

        //Picker03

        if unidadPrimeraVez{
            let indiceFarmaco = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
            
            let picker03Indice0 = listaFarmacos[indiceFarmaco].picker03Indice01
            let picker03Indice1 = listaFarmacos[indiceFarmaco].picker03Indice02
            let picker03Indice2 = listaFarmacos[indiceFarmaco].picker03Indice03

            let picker03IndiceDefault = [picker03Indice0, picker03Indice1, picker03Indice2]
            UserDefaults.standard.set(picker03IndiceDefault, forKey: "unidadDefault")
            definirValores()
            unidadPrimeraVez = false
            
        }
        else {
            let picker03Indice0 = picker03.selectedRow(inComponent: 0)
            let picker03Indice1 = picker03.selectedRow(inComponent: 1)
            let picker03Indice2 = picker03.selectedRow(inComponent: 2)
            let picker03IndiceDefault = [picker03Indice0, picker03Indice1, picker03Indice2]
            UserDefaults.standard.set(picker03IndiceDefault, forKey: "unidadDefault")
            definirValores()
        }


        //definirValores()
        
    }
    
    func definirValores(){
       // Valores dependientes del Picker01
        let indiceFarmaco = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
        
        let nombreFarmaco = listaFarmacos[indiceFarmaco].nombre
        cantidadValue = listaFarmacos[indiceFarmaco].cantidad
        
        let picker02Indice0 = listaFarmacos[indiceFarmaco].picker02Indice01
        switch picker02Indice0 {
            case 0:
                unidadValue = ""
            case 1:
                unidadValue = "mcg"
            case 2:
                unidadValue = "mg"
            case 3:
                unidadValue = "g"
            default:
                break
            }
   
        volumenValue = listaFarmacos[indiceFarmaco].volumenFarmaco + listaFarmacos[indiceFarmaco].volumenDisolucion
            farmacoTextField.text = nombreFarmaco
        cantidadFarmaco.text = String(format: "%.1f",  cantidadValue) + " " + unidadValue
        volumenDilucion.text = String(format: "%0.0f", volumenValue) + " mL"
        
        // Valores dependientes del Picker03. Se coloca antes para los cálculos dependientes de la Unidad
        let respuesta03 = obtenerUnidad()
        unidadesTextField.text = respuesta03
        
        
        // Valores dependientes del Picker02
        let respuesta01 = obtenerValorDeArray(forKey: "pesoDefault")
        pesoValue = respuesta01.0
        pesoTextField.text = String(format: "%.1f", pesoValue) + " kg"
        UserDefaults.standard.set(respuesta01.1, forKey: "pesoDefault")
        switch dosisVelocidad {
            case 1:
                let respuesta01 = obtenerValorDeArray(forKey: "dosisDefault")
                dosisValue = respuesta01.0
                dosisTextField.text = String(format: "%.3f", dosisValue)
                UserDefaults.standard.set(respuesta01.1, forKey: "dosisDefault")
                mlHoraTextField.text = ""

                if unidadValida {
                    let respuesta02 = calculoDosisVelocidad()
                    mlHoraTextField.text = String(format: "%.3f", respuesta02.0)
                    UserDefaults.standard.set(respuesta02.1, forKey: "velocidadDefault")}

            case 0:
                let respuesta01 = obtenerValorDeArray(forKey: "velocidadDefault")
                velocidadValue = respuesta01.0
                mlHoraTextField.text = String(format: "%.3f", velocidadValue)
                UserDefaults.standard.set(respuesta01.1, forKey: "velocidadDefault")
                      dosisTextField.text = ""
                if unidadValida {
                    let respuesta02 = calculoDosisVelocidad()
                    dosisTextField.text = String(format: "%.3f", respuesta02.0)
                    UserDefaults.standard.set(respuesta02.1, forKey: "dosisDefault")}
            default:
                break
        }
        let valoresParaCalculos = [pesoValue, dosisValue, velocidadValue]
        UserDefaults.standard.set(valoresParaCalculos, forKey: "valoresParaCalculos")
        print("Valores para cáculos: \(pesoValue)   \(dosisValue)   \(velocidadValue)")
 
        situarPicker()
    }

    func obtenerValorDeArray(forKey lista: String) -> (Double, Array<Any>){
        let array = UserDefaults.standard.array(forKey: lista) ?? [0, 0, 0, 0]
        var nuevoArray: [Int] = []
        let indice_0 = array[0] as! Int
        let indice_1 = array[1] as! Int
        let indice_2 = array[2] as! Int
        let indice_3 = array[3] as! Int

        
        var valor: String = ""
        
        switch (indice_0, indice_1, indice_2, indice_3){
            case (2..., 1..., 1..., 1...):
                valor = "\(indice_0 - 1).\(indice_1 - 1)\(indice_2 - 1)\(indice_3 - 1)"
                nuevoArray = [indice_0, indice_1, indice_2, indice_3]
            case (2..., 1..., 1..., 0):
                valor = "\(indice_0 - 1).\(indice_1 - 1)\(indice_2 - 1)"
                nuevoArray = [indice_0, indice_1, indice_2, 0]
            case (2..., 1..., 0, 0...):
                valor = "\(indice_0 - 1).\(indice_1 - 1)"
                nuevoArray = [indice_0, indice_1, 0, 0]
            case (2..., 0, 0..., 0...):
                valor = "\(indice_0 - 1)"
                nuevoArray = [indice_0, 0, 0, 0]

            case (0...1, 1..., 1..., 1...):
                valor = "0.\(indice_1 - 1)\(indice_2 - 1)\(indice_3 - 1)"
                nuevoArray = [1, indice_1, indice_2, indice_3]
            case (0...1, 1..., 1..., 0):
                valor = "0.\(indice_1 - 1)\(indice_2 - 1)"
                nuevoArray = [1, indice_1, indice_2, 0]
            case (0...1, 1..., 0, 0...):
                valor = "0.\(indice_1 - 1)"
                nuevoArray = [1, indice_1, 0, 0]
            case (0...1, 0, 0..., 0...):
                valor = ""
                nuevoArray = [0, 0, 0, 0]

            default:
                break}

        if valor != ""{
            valorNumerico = Double(valor)!}
        else {
            valorNumerico = 9999.999
            nuevoArray = [10000, 10, 10, 10]
        }
        return (valorNumerico, nuevoArray)
    }
    func obtenerUnidad() -> (String){
        var texto: String = ""
        let array = UserDefaults.standard.array(forKey: "unidadDefault") ?? [] as! [Int]

        let indice_0 = array[0] as! Int
        let indice_1 = array[1] as! Int
        let indice_2 = array[2] as! Int
        
        let valor0 = unidad[indice_0]
        let valor1 = unidadReferencia[indice_1]
        let valor2 = tiempo[indice_2]
        
        switch (indice_0, indice_1, indice_2) {
            case (1..., 1, 1...):
                texto = "\(valor0)/\(valor1)/\(valor2)"
                unidadValida = true
            case (1..., 0, 1...):
                texto = "\(valor0)/\(valor2)"
                unidadValida = true
            default:
                texto = ""
                unidadValida = false
    }
        return (texto)
    }
    
    func situarPicker() {
        //picker01
        let indice01_0 = UserDefaults.standard.integer(forKey: "posicionFarmacoDefault")
        picker01.selectRow(indice01_0, inComponent: 0, animated: true)
        print("indice01_0: \(indice01_0)")
        //picker02
        var lista:String = ""
        switch marcadorCampo {
            case "Peso":
                lista = "pesoDefault"
            case "Dosis":
                lista = "dosisDefault"
            case "mL/h":
                lista = "velocidadDefault"
            default:
                break
        }
        let array02 = UserDefaults.standard.array(forKey: lista) ?? [0,0,0,0] as! [Int]
        let indice02_0 = array02[0] as! Int
        let indice02_1 = array02[1] as! Int
        let indice02_2 = array02[2] as! Int
        let indice02_3 = array02[3] as! Int
        if indice02_0 <= 10000 {
            picker02.selectRow(indice02_0, inComponent: 0, animated: true)
            picker02.selectRow(indice02_1, inComponent: 1, animated: true)
            picker02.selectRow(indice02_2, inComponent: 2, animated: true)
            picker02.selectRow(indice02_3, inComponent: 3, animated: true)
              }
        else {
            picker02.selectRow(0, inComponent: 0, animated: true)
            picker02.selectRow(0, inComponent: 1, animated: true)
            picker02.selectRow(0, inComponent: 2, animated: true)
            picker02.selectRow(0, inComponent: 3, animated: true)
        }
        //picker03
        let array03 = UserDefaults.standard.array(forKey: "unidadDefault") ?? [0,0,0]
        let indice03_0 = array03[0] as! Int
        let indice03_1 = array03[1] as! Int
        let indice03_2 = array03[2] as! Int
        
        picker03.selectRow(indice03_0, inComponent: 0, animated: true)
        picker03.selectRow(indice03_1, inComponent: 1, animated: true)
        picker03.selectRow(indice03_2, inComponent: 2, animated: true)
      

    }
    func calculoDosisVelocidad() -> (Double, Array<Any>){
 
        let unidades = UserDefaults.standard.array(forKey: "unidadDefault") ?? [0,0,0]
        let indice_0 = unidades[0] as! Int
        let indice_1 = unidades[1] as! Int
        let indice_2 = unidades[2] as! Int
        switch (indice_0,indice_1,indice_2) {

            case (1,1,1): //"mcg/kg/minuto":
                correctorDosisVelocidad01 = 1000 / (60 * pesoValue)
                correctorDosisVelocidad02 = 60 * pesoValue / 1000
            case (1,1,2): //"mcg/kg/hora":
                correctorDosisVelocidad01 = 1000 / pesoValue
                correctorDosisVelocidad02 = pesoValue / 1000
            case (1,1,3): //"mcg/kg/día":
                correctorDosisVelocidad01 = 1000 * 24 / pesoValue
                correctorDosisVelocidad02 = pesoValue / (1000 * 24)
            case (1,0,1): //"mcg/minuto":
                correctorDosisVelocidad01 = 1000 / 60
                correctorDosisVelocidad02 = 60 / 1000
            case (1,0,2): //"mcg/hora":
                correctorDosisVelocidad01 = 1000
                correctorDosisVelocidad02 = 0.001
            case (1,0,3): //"mcg/día":
                correctorDosisVelocidad01 = 1000 * 24
                correctorDosisVelocidad02 = 1 / (1000 * 24)
            case (2,1,1): //"mg/kg/minuto":
                correctorDosisVelocidad01 = 1 / (60 * pesoValue)
                correctorDosisVelocidad02 = 60 * pesoValue
            case (2,1,2): //"mg/kg/hora":
                correctorDosisVelocidad01 = 1 / pesoValue
                correctorDosisVelocidad02 = pesoValue
            case (2,1,3): //"mg/kg/día":
                correctorDosisVelocidad01 = 24 / pesoValue
                correctorDosisVelocidad02 = pesoValue / 24
            case (2,0,1): //"mg/minuto":
                correctorDosisVelocidad01 = 1 / 60
                correctorDosisVelocidad02 = 60
            case (2,0,2):// "mg/hora"
                correctorDosisVelocidad01 = 1
                correctorDosisVelocidad02 = 1
            case (2,0,3): //"mg/día":
                correctorDosisVelocidad01 = 24
                correctorDosisVelocidad02 = 1 / 24
            case (3,1,1): //"g/kg/minuto":
                correctorDosisVelocidad01 =  0.001 / (60 * pesoValue)
                correctorDosisVelocidad02 = 1000 * 60 * pesoValue
            case (3,1,2): //"g/kg/hora":
                correctorDosisVelocidad01 = 0.001 / pesoValue
                correctorDosisVelocidad02 = 1000 * pesoValue
            case (3,1,3): //"g/kg/día":
                correctorDosisVelocidad01 = 0.001 * 24 / pesoValue
                correctorDosisVelocidad02 = 1000 *  pesoValue / 24
            case (3,0,1): //"g/minuto":
                correctorDosisVelocidad01 = 0.001 / 60
                correctorDosisVelocidad02 = 1000 * 60
            case (3,0,2): //"g/hora":
                correctorDosisVelocidad01 = 0.001
                correctorDosisVelocidad02 = 1000
            case (3,0,3): //"g/día":
                correctorDosisVelocidad01 = 0.001 * 24
                correctorDosisVelocidad02 = 1000 / 24
            default:
                break
        }
        
        switch unidadValue {
            case "g":
                cantidadCalculos = cantidadValue * 1000
            case "mcg":
                cantidadCalculos = cantidadValue * 0.001
            default:
                cantidadCalculos = cantidadValue
        }
      
        if dosisVelocidad == 0{
            dosisValue = correctorDosisVelocidad01 * velocidadValue * cantidadCalculos / volumenValue
            dosisTextField.text = String(format: "%.3f", dosisValue)
            let dosis = String(format: "%.3f", dosisValue)
            var posicionPicker: [Int] = []
            var index = 0
            if dosis.contains("."){
                let puntoPosicion = dosis.index(of: ".")!
                let numeroInicial = dosis.prefix(upTo: puntoPosicion)
                let posicionInicial = (Int(numeroInicial)! + 1)
                // posicionPicker.append(posicionInicial)
                
                for i in (0...pesoEntero.count - 1){
                    if pesoEntero[i] == String(numeroInicial){
                        posicionPicker.append(i)}
                }
                index = 1
                for  i in 0...2{
                    var posicion = dosis.index(puntoPosicion, offsetBy: index)
                    let valor = dosis[posicion]
                    let numero  = String(valor)
                    for i in (0...pesoDecimal.count - 1){
                        if pesoDecimal[i] == numero{
                            posicionPicker.append(i)}
                    }
                    index = index + 1
                }
                UserDefaults.standard.set(posicionPicker, forKey: "dosisDefault")
            }
            if dosisValue > 9999 {
                posicionPicker = [10000, 10, 10, 10]
            }
            return (dosisValue, posicionPicker)
        }
        
    
        else {
            velocidadValue = correctorDosisVelocidad02 * dosisValue * volumenValue / cantidadValue
            mlHoraTextField.text = String(format: "%.3f", dosisValue)
            mlHoraTextField.text = String(format: "%.2f", velocidadValue)
            let velocidad = String(format: "%.3f", velocidadValue
                )
            var posicionPicker: [Int] = []
            var index = 0
            
            if velocidad.contains("."){
                let puntoPosicion = velocidad.index(of: ".")!
                let numeroInicial = velocidad.prefix(upTo: puntoPosicion)
                let posicionInicial = (Int(numeroInicial)! + 1)
                // posicionPicker.append(posicionInicial)
                
                for i in (0...pesoEntero.count - 1){
                    if pesoEntero[i] == String(numeroInicial){
                        posicionPicker.append(i)}
                }
                index = 1
                for  i in 0...2{
                    var posicion = velocidad.index(puntoPosicion, offsetBy: index)
                    let valor = velocidad[posicion]
                    let numero  = String(valor)
                    for i in (0...pesoDecimal.count - 1){
                        if pesoDecimal[i] == numero{
                            posicionPicker.append(i)}
                    }
                    index = index + 1
                }
                UserDefaults.standard.set(posicionPicker, forKey: "velocidadDefault")
            }
            if velocidadValue > 9999 {
                posicionPicker = [10000, 10, 10, 10]}
            return (velocidadValue, posicionPicker)

        }
    }
    

    @objc func refrescarPantalla(notification: NSNotification){
        //self.viewDidLoad()
        self.viewWillAppear(true)
        self.picker01.reloadAllComponents()
        self.picker02.reloadAllComponents()
        self.picker03.reloadAllComponents()

        
        definirValores()
    }
    /*func viewForZooming(in ScrollView: UIScrollView) -> UIView? {
        return self.contenedorView
    }
 */
    
    override func viewDidLoad() {
       super.viewDidLoad()
        
        /* Inicio método Pinch zoom. Requiere la scrollView delegate"
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3
        scrollView.contentSize.height = 1000
        Fin método Pinch zoom"
        */

        
        //Desbloquea le programa cuando no se cargan los valores por defecto
        recargaEmergencia()
        
        // Carga una notificación para refrescar la pantalla cuando se vuelve de la pantalla PopUp. La notificación se llama RecargaVista y llama a la función refrescarPantalla, que es la que se encarga de refrescar la pantalla. La notificación se ejecuta desde la pantalla PopUp, al cerrar la misma
        NotificationCenter.default.addObserver(self, selector: #selector(refrescarPantalla), name: NSNotification.Name(rawValue: "RecargarVista"), object: nil)
        
        // Inicializar el array de valores hasta 9999 y hasta 09. El valor "" esta incluido al definir la variable
        for i in (0...9999){
            let valor = String(i)
            pesoEntero.append(valor)
        }
        for i in (0...9){
            let valor = String(i)
            pesoDecimal.append(valor)
        }
     
        // Valores basales
        // Se pone un marcador para que sólo se arranquen al inicio y no cuando se refresca la pantalla
        if cargarDatosDeNuevo {
            let peso = [71,0,0,0]
                UserDefaults.standard.set(peso, forKey: "pesoDefault")
            let dosis = [6,0,0,0]
                UserDefaults.standard.set(dosis, forKey: "velocidadDefault")
            let unidad = [1,1,1]
                UserDefaults.standard.set(unidad, forKey: "unidadDefault")
            let farmacoElegido = 0
                UserDefaults.standard.set(farmacoElegido, forKey: "posicionFarmacoDefault")
            cargarDatosDeNuevo = false
        }
        //////////////////////////
        
        for vistas in contenedorView.subviews {
            if (vistas is UIView) {
            }
            vistas.layer.borderColor = colorAzul.cgColor
            vistas.layer.borderWidth = 3}
        for vistas in marcoPicker03.subviews {
            if (vistas is UIView) {
            }
            vistas.layer.borderColor = colorAzul.cgColor
            vistas.layer.borderWidth = 2}
        unidadesTextField.text = ""
        farmacoTextField.isUserInteractionEnabled = false
        cantidadFarmaco.isUserInteractionEnabled = false
        volumenDilucion.isUserInteractionEnabled = false
        unidadesTextField.isUserInteractionEnabled = false
        
        
        picker01.delegate = self
        picker01.dataSource = self
        picker02.delegate = self
        picker02.dataSource = self
        picker03.delegate = self
        picker03.dataSource = self
        
        let farmacosOut = UserDefaults.standard.data(forKey: "FarmacosDefault")
        let lista = try! JSONDecoder().decode([Farmaco].self, from: farmacosOut!)
        listaFarmacos = lista
        
 
 
        
        self.view.addSubview(picker01)
        picker01.translatesAutoresizingMaskIntoConstraints = false
        picker01.leadingAnchor.constraint(equalTo: marcoPicker01.leadingAnchor, constant: 10).isActive = true
        picker01.trailingAnchor.constraint(equalTo: marcoPicker01.trailingAnchor, constant: -10).isActive = true
        
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker01,
                attribute: .top,
                relatedBy: .equal,
                toItem: picker01,
                attribute: .top,
                multiplier: 1.0,
                constant: 0))
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker01,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: picker01,
                attribute: .bottom,
                multiplier: 1.0,
                constant: 0))
        
        self.view.addSubview(picker02)
        picker02.translatesAutoresizingMaskIntoConstraints = false
        picker02.leadingAnchor.constraint(equalTo: marcoPicker02.leadingAnchor, constant: 10).isActive = true
        picker02.trailingAnchor.constraint(equalTo: marcoPicker02.trailingAnchor, constant: -10).isActive = true
        
        
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker02,
                attribute: .top,
                relatedBy: .equal,
                toItem: picker02,
                attribute: .top,
                multiplier: 1.0,
                constant: -30))
        self.view.addConstraint(
            NSLayoutConstraint(
                item: marcoPicker02,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: picker02,
                attribute: .bottom,
                multiplier: 1.0,
                constant: 0))
        
        
        self.view.addSubview(picker03)
        picker03.translatesAutoresizingMaskIntoConstraints = false
        
       // picker03.leadingAnchor.constraint(equalTo: marcoPicker03.leadingAnchor, constant: marcoPicker03.frame.width * 0.41).isActive = true
       // picker03.trailingAnchor.constraint(equalTo: marcoPicker03.trailingAnchor, constant: -10).isActive = true
       // picker03.topAnchor.constraint(equalTo: marcoPicker03.topAnchor, constant: 30).isActive = true
        
        self.view.addConstraint(
        NSLayoutConstraint (item: picker03,
                attribute: .width,
                relatedBy: .equal,
                toItem: marcoPicker03,
                attribute: .width,
                multiplier: 0.55,
                constant: 0))
        self.view.addConstraint(
        NSLayoutConstraint (item: picker03,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: marcoPicker03,
                attribute: .centerX,
                multiplier: 1.4,
                constant: 0))
        self.view.addConstraint(
        NSLayoutConstraint (item: picker03,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: marcoPicker03,
                attribute: .centerY,
                multiplier: 1,
                constant: 0))
        

        marcoPicker02.backgroundColor = colorGrisClaro
    
       definirValores()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        for view in marcoPicker03.subviews {
            if (view is UITextField) {
                let textField = view as! UITextField
                textField.backgroundColor = .white}
        }
        pesoTextField.backgroundColor = .white
        dosisTextField.backgroundColor = .white
        mlHoraTextField.backgroundColor = .white
        
        textField.backgroundColor = UIColor(red: 210/255, green: 232/255, blue: 208/255, alpha: 1.0)
       textField.endEditing(true)
        
 
        
        if textField == pesoTextField &&  dosisVelocidad == 0 {
            dosisTextField.backgroundColor = colorAzulClaro
        }
        else if textField == pesoTextField &&  dosisVelocidad == 1 { mlHoraTextField.backgroundColor = colorAzulClaro
        }
 
}

    @IBAction func abriPopUp(_ sender: Any) {
    let PopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PerfusionesPopUp") as! PerfusionesPopUp
    self.addChild(PopUp)
    PopUp.view.frame = self.view.frame
    self.view.addSubview(PopUp.view)
    PopUp.didMove(toParent: self)
}

   
    @IBAction func resetValores(_ sender: Any) {
        var valores = [71,0,0,0]
            UserDefaults.standard.set(valores, forKey: "pesoDefault")
        valores = [1, 1, 1]
            UserDefaults.standard.set(valores, forKey: "unidadDefault")
        valores = [6,0,0,0]
            UserDefaults.standard.set(valores, forKey: "velocidadDefault")
            dosisVelocidad = 0
        valores = [0]
            UserDefaults.standard.set(valores, forKey: "posicionFarmacoDefault")
        definirValores()
    }

    func recargaEmergencia() {
        let Noradrenalina = Farmaco(nombre: "Noradrenalina", cantidad: 20, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 100, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Dobutamina = Farmaco(nombre: "Dobutamina", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Dopamina = Farmaco(nombre: "Dopamina", cantidad: 200, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 45, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Adrenalina = Farmaco(nombre: "Adrenalina", cantidad: 5, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 100, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Urapidilo = Farmaco(nombre: "Urapidilo", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 250, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Levosimendan = Farmaco(nombre: "Levosimendan", cantidad: 12.5, picker02Indice01: 2, volumenFarmaco: 5, volumenDisolucion: 500, dosisRecomendada: "0.05 - 0.2 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Labetalol = Farmaco(nombre: "Labetalol", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "2 - 10 mg/min", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 1)
        let Nitroglicerina = Farmaco(nombre: "Nitroglicerina", cantidad: 10, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 0, picker03Indice03: 1)
        let Nimodipino = Farmaco(nombre: "Nimodipino", cantidad: 10, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "2 mg/hora", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
        let Propofol = Farmaco(nombre: "Propofol", cantidad: 1000, picker02Indice01: 2, volumenFarmaco: 50, volumenDisolucion: 0, dosisRecomendada: "4 - 8 mg/kg/hora", picker03Indice01: 2, picker03Indice02: 1, picker03Indice03: 2)
        let Midazolam = Farmaco(nombre: "Midazolam", cantidad: 100, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "0.03 - 0.2 mg/kg/hora", picker03Indice01: 2, picker03Indice02: 1, picker03Indice03: 2)
        let Dexmedetomidina = Farmaco(nombre: "Dexmedetomidina", cantidad: 20, picker02Indice01: 1, volumenFarmaco: 2, volumenDisolucion: 48, dosisRecomendada: "0.7 - 1.4 mcg/kg/hora", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 2)
        let Remifentanilo = Farmaco(nombre: "Remifentanilo", cantidad: 2, picker02Indice01: 2, volumenFarmaco: 2, volumenDisolucion: 48, dosisRecomendada: "0.5 - 1.0 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let CloruroMorfico = Farmaco(nombre: "Cloruro Mórfico", cantidad: 50, picker02Indice01: 2, volumenFarmaco: 3, volumenDisolucion: 47, dosisRecomendada: "0.8 - 10 mg/hora", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
        let Cisatracurio = Farmaco(nombre: "Cisatracurio", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 20, volumenDisolucion: 30, dosisRecomendada: "", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Fentanilo = Farmaco(nombre: "Fentanilo", cantidad: 750, picker02Indice01: 1, volumenFarmaco: 3, volumenDisolucion: 47, dosisRecomendada: "1 - 5 mcg/kg/min", picker03Indice01: 1, picker03Indice02: 1, picker03Indice03: 1)
        let Furosemida = Farmaco(nombre: "Furosemida", cantidad: 250, picker02Indice01: 2, volumenFarmaco: 25, volumenDisolucion: 25, dosisRecomendada: "", picker03Indice01: 2, picker03Indice02: 0, picker03Indice03: 2)
        
        
        
        var lista = [Noradrenalina, Dobutamina, Dopamina, Adrenalina, Urapidilo, Levosimendan, Labetalol, Nitroglicerina, Nimodipino, Propofol,Midazolam, Dexmedetomidina, Remifentanilo, CloruroMorfico, Cisatracurio, Fentanilo, Furosemida]
        lista = lista.sorted(by: {$0.nombre < $1.nombre})
        let farmacosIn = try! JSONEncoder().encode(lista)
        UserDefaults.standard.set(farmacosIn, forKey: "FarmacosDefault")
        /*
        for valores in lista{
            print("Nombre: \(valores.nombre)  Cantidad: \(valores.cantidad)")}
 */
    }
}
