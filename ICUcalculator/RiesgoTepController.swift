//
//  RiesgoTepController.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 24/11/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class RiesgoTepController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var principalView: UIView!
    @IBOutlet var contenedorView: UIView!
    @IBOutlet var barraSuperior: UIToolbar!
    @IBOutlet var marcoPrincipalView: UIView!
    @IBOutlet var accionesView: UIView!

    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var riesgoHorizontalLabel: UILabel!
    @IBOutlet var accionesLabel: UILabel!
    
    var rectangulo: UIView!
    var gradiente: CAGradientLayer!
    var etiqueta: UILabel!
    var etiquetaVertical: UILabel!
    var etiquetaV: String = ""
    var derechaConstraint: NSLayoutConstraint!
    var izquierdaConstraint: NSLayoutConstraint!
    var superiorConstraint: NSLayoutConstraint!
    var inferiorConstraint: NSLayoutConstraint!
    var anchuraConstraint: NSLayoutConstraint!
    var alturaConstraint: NSLayoutConstraint!
   
    var verdeInferiorColor: CGColor = UIColor(red: 51/255, green: 204/255, blue: 0/255, alpha: 1.0).cgColor
    var verdeSuperiorColor: CGColor = UIColor(red: 4/255, green: 206/255, blue: 44/255, alpha: 1.0).cgColor
    var amarilloInferiorColor: CGColor = UIColor(red: 200/255, green: 204/255, blue: 0/255, alpha: 1.0).cgColor
    var amarilloSuperiorColor: CGColor = UIColor(red: 252/255, green: 249/255, blue: 60/255, alpha: 1.0).cgColor
    var naranjaInferiorColor: CGColor = UIColor(red: 214/255, green: 146/255, blue: 51/255, alpha: 1.0).cgColor
    var naranjaSuperiorColor: CGColor = UIColor(red: 255/255, green: 127/255, blue: 0/255, alpha: 1.0).cgColor
    var rojoInferiorColor: CGColor = UIColor(red: 214/255, green: 146/255, blue: 51/255, alpha: 1.0).cgColor
    var rojoSuperiorColor: CGColor = UIColor(red: 252/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
    
    var colorAzul: UIColor = UIColor(red: 0/255, green: 74/255, blue: 211/255, alpha: 1.0)
    var colorFondoPicker: UIColor = UIColor(red: 173/255, green: 218/255, blue: 247/255, alpha: 1.0)
    
    @IBOutlet var switchColection: [UISwitch]!
 
    
    @IBOutlet var escalaColorView: UIView!
    var shock: Int = 0
    var ventriculoDerecho: Int = 0
    var marcadores: Int = 0
    var ordenAparicion: Int = 0
    var posicion: String = "Portrait"
    var repetidor: Timer!
    var intervalo: Double = 2.0

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        print("Will Transition to size \(size) from super view size \(self.view.frame.size)")
        
        if (size.width > self.view.frame.size.width) {
            print("Landscape")
            posicion = "Landscape"
            rectangulo.removeFromSuperview()
            etiquetaVertical.removeFromSuperview()
            
            
        } else {
            print("Portrait")
            posicion = "Portrait"
            rectangulo.removeFromSuperview()
            etiqueta.removeFromSuperview()
            
            
        }
        
        crearRectangulo()
    }
   
    func crearRectangulo(){
        rectangulo = UIView()
        gradiente = CAGradientLayer()
        
       // gradiente.colors = [verdeSuperiorColor, amarilloInferiorColor]
       // gradiente.locations = [0, 0.5, 1]
        etiqueta = UILabel()
        etiquetaVertical = UILabel()
        
        rectangulo.backgroundColor = UIColor.red
        // La siguiente línea es necesaria para que funcionen las constraints
        self.view.addSubview(rectangulo)
        self.view.addSubview(etiqueta)
        self.view.addSubview(etiquetaVertical)
       
        // Sin las tres líneas siguientes no funcionan las constraints por código.
        rectangulo.translatesAutoresizingMaskIntoConstraints = false
        etiqueta.translatesAutoresizingMaskIntoConstraints = false
        etiquetaVertical.translatesAutoresizingMaskIntoConstraints = false

        superiorConstraint = rectangulo.topAnchor.constraint(equalTo: marcoPrincipalView.topAnchor, constant: 20)
        inferiorConstraint = rectangulo.bottomAnchor.constraint(equalTo: marcoPrincipalView.bottomAnchor, constant: -20)
        derechaConstraint = rectangulo.trailingAnchor.constraint(equalTo: marcoPrincipalView.trailingAnchor, constant: -20)
        izquierdaConstraint = rectangulo.leadingAnchor.constraint(equalTo: marcoPrincipalView.leadingAnchor, constant: 20)
        anchuraConstraint = rectangulo.widthAnchor.constraint(equalToConstant: 60)
        alturaConstraint = rectangulo.heightAnchor.constraint(equalToConstant: 60)
        
        if posicion == "Portrait"{
            alturaConstraint.isActive = false
            izquierdaConstraint.isActive = false
            superiorConstraint.isActive = true
            inferiorConstraint.isActive = true
            derechaConstraint.isActive = true
            anchuraConstraint.isActive = true
            
            etiquetaVertical.trailingAnchor.constraint(equalTo: marcoPrincipalView.trailingAnchor, constant: -30).isActive = true
            etiquetaVertical.topAnchor.constraint(equalTo: marcoPrincipalView.topAnchor, constant: 12).isActive = true
            etiquetaVertical.bottomAnchor.constraint(equalTo: marcoPrincipalView.bottomAnchor, constant: -12).isActive = true
            etiquetaVertical.widthAnchor.constraint(equalToConstant: 40).isActive = true
            
            
            etiqueta.removeFromSuperview()
            etiquetaVertical.numberOfLines = 0
            etiquetaVertical.textAlignment = .center
            etiquetaVertical.textColor = .black
            etiquetaVertical.font = accionesLabel.font.withSize(15)
        }
        else if posicion == "Landscape" {
            superiorConstraint.isActive = false
            anchuraConstraint.isActive = false
            alturaConstraint.isActive = true
            inferiorConstraint.isActive = true
            izquierdaConstraint.isActive = true
            derechaConstraint.isActive = true
            
            etiqueta.trailingAnchor.constraint(equalTo: marcoPrincipalView.trailingAnchor, constant: -30).isActive = true
            etiqueta.leadingAnchor.constraint(equalTo: marcoPrincipalView.leadingAnchor, constant: 30).isActive = true
            etiqueta.bottomAnchor.constraint(equalTo: marcoPrincipalView.bottomAnchor, constant: -30).isActive = true
            etiqueta.heightAnchor.constraint(equalToConstant: 40).isActive = true

            etiquetaVertical.removeFromSuperview()
            etiqueta.numberOfLines = 1
            etiqueta.textAlignment = .center
            etiqueta.textColor = .black
            //etiqueta.text = "RIESGO INTERMEDIO - ALTO"
        }
        colorBarra()
        
    }
    
    func viewForZooming(in ScrollView: UIScrollView) -> UIView? {
        return self.contenedorView
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Inicio método Pinch zoom. Requiere la scrollView delegate"
       self.scrollView.minimumZoomScale = 1.0
       self.scrollView.maximumZoomScale = 3
       scrollView.contentSize.height = 1000
       // Fin método Pinch zoom"
        
        super.viewDidLoad()
        repetidor = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(parpadeo), userInfo: nil, repeats: true)
        
        for valores in switchColection {
            valores.tintColor = colorAzul
            valores.onTintColor = colorAzul
            valores.isOn = false
    }
        marcoPrincipalView.layer.borderWidth = 2
        marcoPrincipalView.layer.borderColor = colorAzul.cgColor
        marcoPrincipalView.backgroundColor = .white
        accionesView.layer.borderWidth = 2
        accionesView.layer.borderColor = colorAzul.cgColor
        accionesView.backgroundColor = .white
        accionesLabel.font = accionesLabel.font.withSize(25)
        accionesLabel.alpha = 0.5
        accionesLabel.text = "Tratamiento"
 
        crearRectangulo()
}
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
     

        gradiente.frame = rectangulo.bounds
        self.contenedorView.addSubview(rectangulo)
        rectangulo.layer.addSublayer(gradiente)
        self.contenedorView.addSubview(etiqueta)
        self.contenedorView.addSubview(etiquetaVertical)
    }
       
        @objc func parpadeo(){
            UIView.animate(withDuration: intervalo, delay: intervalo, options: .curveEaseIn, animations: {
                self.etiqueta.alpha = 0.1
                self.etiquetaVertical.alpha = 0.1

            }) { _ in
                UIView.animate(withDuration: self.intervalo, delay: 0, options: .curveEaseIn, animations: {
                    self.etiqueta.alpha = 1
                    self.etiquetaVertical.alpha = 1

                })
            }
        }
    
    @IBAction func shockSwitch(_ sender: UISwitch) {
        if(sender.isOn == true) {
            shock = 1
        }
        else{
            shock = 0
            }
        colorBarra()
        }
    
    
    @IBAction func ventriculoDerechoSwitch(_ sender: UISwitch) {
        if(sender.isOn == true) {
            ventriculoDerecho = 1
        }
        else{
            ventriculoDerecho = 0
        }
    colorBarra()
    }
    
    @IBAction func biomarcadoresSwich(_ sender: UISwitch) {
        if(sender.isOn == true) {
            marcadores = 1
        }
        else{
            marcadores = 0
        }
        colorBarra()
    }
    func colorBarra(){
        switch posicion {
        case "Portrait":

            accionesLabel.alpha = 1
            gradiente = CAGradientLayer()
            gradiente.frame = rectangulo.bounds
            gradiente.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradiente.endPoint = CGPoint(x: 0.0, y: 1.0)
           
            switch (marcadores, ventriculoDerecho, shock) {
            case (0, 0, 0):
                //Verde
                gradiente.removeFromSuperlayer()
                gradiente.colors = [verdeSuperiorColor, verdeInferiorColor]
                gradiente.locations = [0, 0.5, 1]
                rectangulo.layer.insertSublayer(gradiente, at: 10)
               
                hacerEtiquetaVertical(riesgo: "RIESGO BAJO")
                etiquetaVertical.text = etiquetaV
                intervalo = 1.5
               
               
                accionesLabel.font = accionesLabel.font.withSize(17)
                 accionesLabel.text = "Ingreso en Sº de Observación de Urgencias o planta de hospitalización, a criterio del médico responsable del Sº de Urgencias"
      
            case (1, 0 ,0):
                
                //Amarillo
                gradiente.removeFromSuperlayer()
                gradiente.colors = [amarilloSuperiorColor, amarilloInferiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.5, 0.75, 1]
               // rectangulo.layer.addSublayer(gradiente)
                 rectangulo.layer.insertSublayer(gradiente, at: 10)
                hacerEtiquetaVertical(riesgo: "RIESGO INTERMEDIO BAJO")
                etiquetaVertical.text = etiquetaV
               intervalo = 1.2
                accionesLabel.font = accionesLabel.font.withSize(17)
               
                    accionesLabel.text = "Ingreso en Sº de Observación de Urgencias o planta de hospitalización, a criterio del médico responsable del Sº de Urgencias"
        
            case (0...1, 1, 0):
                //Naranja
                gradiente.removeFromSuperlayer()
                gradiente.colors = [naranjaSuperiorColor, naranjaInferiorColor, amarilloSuperiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.33, 0.55, 1]
                //rectangulo.layer.addSublayer(gradiente)
                 rectangulo.layer.insertSublayer(gradiente, at: 10)
                hacerEtiquetaVertical(riesgo: "RIESGO INTERMEDIO ALTO")
                etiquetaVertical.text = etiquetaV
                intervalo = 0.9
                accionesLabel.font = accionesLabel.font.withSize(17)
                
                    accionesLabel.text = "No se recomienda ninguna estrategia de reperfusión de forma rutinaria\nHeparina sódica\nTraslado a UCI / UCC / Observación"

                
            case (0...1, 0...1, 1):
                //Rojo
                gradiente.removeFromSuperlayer()
                gradiente.colors = [rojoSuperiorColor, rojoInferiorColor, naranjaSuperiorColor, amarilloSuperiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.5,0.7, 0.9, 1]
               // rectangulo.layer.addSublayer(gradiente)
                 rectangulo.layer.insertSublayer(gradiente, at: 10)
                hacerEtiquetaVertical(riesgo: "RIESGO ALTO")
                etiquetaVertical.text = etiquetaV
                intervalo = 0.6
                accionesLabel.font = accionesLabel.font.withSize(17)
                accionesLabel.text = "Fibrinolisis o\n Trombectomía percutánea\nHeparina sódica\nTraslado a UCI / UCC"
         
            default:
                break
            }
        case "Landscape":
            accionesLabel.alpha = 1
            gradiente = CAGradientLayer()
            gradiente.frame = rectangulo.bounds
            gradiente.startPoint = CGPoint(x: 1.0, y: 0.0)
            gradiente.endPoint = CGPoint(x: 0.0, y: 0.0)
            
            
            
            switch (marcadores, ventriculoDerecho, shock) {
            case (0, 0, 0):
                //Verde
                gradiente.removeFromSuperlayer()
                gradiente.colors = [verdeSuperiorColor, verdeInferiorColor]
                gradiente.locations = [0, 0.5, 1]
                rectangulo.layer.addSublayer(gradiente)
                etiqueta.text = "RIESGO BAJO"
                accionesLabel.font = accionesLabel.font.withSize(17)
                accionesLabel.text = "Ingreso en Sº de Observación de Urgencias o planta de hospitalización, a criterio del médico responsable del Sº de Urgencias"
                 intervalo = 1.5
            case (1, 0 ,0):
                
                //Amarillo
                gradiente.removeFromSuperlayer()
                gradiente.colors = [amarilloSuperiorColor, amarilloInferiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.5, 0.75, 1]
                rectangulo.layer.addSublayer(gradiente)
               etiqueta.text = "RIESGO INTERMEDIO BAJO"
                accionesLabel.font = accionesLabel.font.withSize(17)
                intervalo = 1.2
                accionesLabel.text = "Ingreso en Sº de Observación de Urgencias o planta de hospitalización, a criterio del médico responsable del Sº de Urgencias"
                
            case (0...1, 1, 0):
                //Naranja
                gradiente.removeFromSuperlayer()
                gradiente.colors = [naranjaSuperiorColor, naranjaInferiorColor, amarilloSuperiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.33, 0.55, 1]
                rectangulo.layer.addSublayer(gradiente)
                etiqueta.text = "RIESGO INTERMEDIO ALTO"
                accionesLabel.font = accionesLabel.font.withSize(17)
                intervalo = 0.9
                accionesLabel.text = "No se recomienda ninguna estrategia de reperfusión de forma rutinaria\nHeparina sódica\nTraslado a UCI / UCC / Observación"
                
                
            case (0...1, 0...1, 1):
                //Rojo
                gradiente.removeFromSuperlayer()
                gradiente.colors = [rojoSuperiorColor, rojoInferiorColor, naranjaSuperiorColor, amarilloSuperiorColor, verdeSuperiorColor]
                gradiente.locations = [0, 0.5,0.7, 0.9, 1]
                rectangulo.layer.addSublayer(gradiente)
                etiqueta.text = "RIESGO ALTO"
                // escalaColorView.layer.insertSublayer(gradiente, at: 0)
                 intervalo = 0.6
                accionesLabel.font = accionesLabel.font.withSize(17)
                accionesLabel.text = "Fibrinolisis o\n Trombectomía percutánea\nHeparina sódica\nTraslado a UCI / UCC"
                
            default:
                break
            }
            
        default:
            break
        }

        
    }
    func hacerEtiquetaVertical(riesgo: String) -> String{
        etiquetaV = riesgo
        var indice: Int = 1
        for char in etiquetaV {
            let posicion = etiquetaV.index(etiquetaV.startIndex, offsetBy: indice) // offsetBy indica la posición a partir del startIndex que se suma
            etiquetaV.insert(contentsOf: "\n", at: posicion)
            indice = indice + 2 // Se suman dos posiciones porque a la cadena se le añade un valor más en cada paso; \n cuenta como un sólo carácter.
        }
        print(etiquetaV)
        return(etiquetaV)
    }
}


