//
//  ViewController.swift
//  Nutricion01
//
//  Created by Julio Barado Hualde on 07/01/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var pesoTF: UITextField!
 
    @IBOutlet var pesoElegidoTF: UITextField!
    @IBOutlet var tallaTF: UITextField!
    @IBOutlet var edadTF: UITextField!
    @IBOutlet var sexoTF: UITextField!
    @IBOutlet var nitrogenoTF: UITextField!
    @IBOutlet var HdeCTF: UITextField!
    @IBOutlet var lipidosTF: UITextField!
    @IBOutlet var calTotalTF: UITextField!
    @IBOutlet var calKgTF: UITextField!
    @IBOutlet var calHarrisTF: UITextField!
    @IBOutlet var HdeCLipidosTF: UITextField!
    
    @IBOutlet var CalNPgN2TF: UITextField!
    var slider0: UISlider = UISlider()
    var slider1: UISlider = UISlider()

    var maximoValorSlider0: Float = 100
    var minimoValorSlider0: Float = 0
    var maximoValorSlider1: Float = 100
    var minimoValorSlider1: Float = 0
    var marcadorCampo: String = ""
    var slider0Posición: Float = 0
    var slider1Posición: Float = 0
    
    var pesoActual: Float = 70
    var pesoIdeal: Float = 70
    var pesoAjustado: Float = 70
    var pesoCalculos: Float!
    var talla: Float = 170
    var edad: Float = 50
    var sexo: String = "Hombre"
    
    var pesoActual0: Float = 0
    var pesoActual1: Float = 0
    
    var pesoAustadol0: Float = 0
    var pesoAustado1: Float = 0
    
    var pesoIdeal0: Float = 0
    var pesoIdeal1: Float = 0
    

    
    var harrisBenedict: Float = 1
    
    var nitrogeno0: Float = 0
    var nitrogeno1: Float = 0
    
    var caloriasTotales: Float = 500
    var caloriasTotales0: Float = 0
    var caloriasTotales1: Float = 0
    var calKg: Float = 1
    var calKg0: Float = 0
    var calKg1: Float = 0

    var calHarris: Float = 1
    var calHarris0: Float = 0
    var calHarris1: Float = 0
    
    var HdeCLipidos: Float = 50
    var HdeCLipidos0: Float = 50
    var HdeCLipidos1: Float = 0

    var HdeC: Float = 1
    var HdeC0: Float = 0
    var HdeC1: Float = 0
    
    var lipidos: Float = 1
    var lipidos0: Float = 0
    var lipidos1: Float = 0
    
    let step:Float=10 // If you want UISlider to snap to steps by 10
    var paso0: Float = 1
    var paso1: Float = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        pesoCalculos = pesoActual
        
        slider()
    }
        
    func slider(){
        self.slider0.removeFromSuperview()
        slider0 = UISlider(frame:CGRect(x: 10, y: 460, width: 300, height: 20))
        slider0.minimumValue = minimoValorSlider0
        slider0.maximumValue = maximoValorSlider0
        slider0.isContinuous = true
        slider0.tintColor = UIColor.green
        slider0.addTarget(self, action: #selector(ViewController.sliderValueDidChange(_:)), for: .valueChanged)
        self.view.addSubview(slider0)
        slider0.setValue(slider0Posición, animated: true)
        print("Slider0Posición: \(slider0Posición)")


        self.slider1.removeFromSuperview()
        slider1 = UISlider(frame:CGRect(x: 10, y: 520, width: 300, height: 20))
        slider1.minimumValue = minimoValorSlider1
        slider1.maximumValue = maximoValorSlider1
        slider1.isContinuous = true
        slider1.tintColor = UIColor.green
        slider1.addTarget(self, action: #selector(ViewController.sliderValueDidChange(_:)), for: .valueChanged)
        self.view.addSubview(slider1)
        slider1.setValue(slider1Posición, animated: true)
        print("Slider1Posición: \(slider1Posición)")

    }
    
    func calculoPeso(peso: Float, talla: Float){
        pesoIdeal = ((talla / 2.54) - 60) * 2.3 + 45.5
        let limiteObesidad = pesoIdeal * 1.3
        pesoAjustado = pesoIdeal + ((peso - pesoIdeal) * 0.25)    }
 
    func harrisBeneditt(peso: Float, talla: Float, edad: Float, sexo: String) -> Float{
        
        if (pesoCalculos != nil && talla != nil && edad != nil && sexo != nil){
            switch sexo {
                case "Hombre":
                    harrisBenedict = 66.47 + (13.75 * pesoCalculos) + (5 * talla) - (6.75 * edad)
                case "Mujer":
                    harrisBenedict = 665.1 + (9.563 * pesoCalculos) + (1.85 * talla) - (4.676 * edad)
                default:
                    break
                }
    }
        /*The versions of the HB equation for men and
        women are as follows, with W equal to weight in kilograms,
        H the height in centimeters, and A the age in
        years: 66.47 + (13.75 × W) + (5 × H) – (6.75 × A) for men
            and 665.1 + (9.563 × W) + (1.85 × H) – (4.676 × A) for
        women. The ideal body weight was calculated from
        the height in meters: 22.5 × H2 for men and 21.5 × H2
        for women. Predicted body weight was calculated
        from the height in centimeters: 50 + 0.91 (H – 152.4)
        for men and 45.5 + 0.91 (H – 152.4) for women.
 */
        return harrisBenedict
    }
    func separarEnteroDecimal(numero: Float) -> (Float, Float){
        let numeroString = String(numero)
        let numeroComponentes = numeroString.components(separatedBy :".")
        let enteroNumero = Float(numeroComponentes [0])
        let decimalNumero = Float("0." + numeroComponentes [1])
        return (enteroNumero!, decimalNumero!)
    }
    func separarPartesNumero(numero: Float, indice: Int) -> (Float, Float){
        let numeroString = String(round(numero))
        let index = numeroString.index(numeroString.endIndex, offsetBy: indice)

        //let index = numeroString.index(numeroString.startIndex, offsetBy: indice)
        let primeraParte = numeroString.prefix(upTo: index)
        let segundaParte = numeroString.suffix(from: index)
        print("numero: \(numeroString). PrimeraParte: \(primeraParte). SegundaParte: \(segundaParte)")

        
        let primerValor = Float(primeraParte)
        let segundoValor = Float(segundaParte)
        return (primerValor ?? 0.0, segundoValor ?? 0.0)
    }
        func separarHBNumero(numero: Float, indice: Int) -> (Float, Float){
         
            let primero: Float = numero * 10
            var primerValor = Float(Int(primero)) / 10
            var segundoValor = round((primero - Float(Int(primero))) * 10)
            
            if segundoValor == 10{
                segundoValor = 0
                primerValor = primerValor + 1
            }
            segundoValor = segundoValor / 100
            print("Valor: \(numero). PrimerValor \(primerValor). SegundoValor: \(segundoValor)")
            return (primerValor ?? 0.0, segundoValor ?? 0.0)

            
           /*
            let numeroString = String(numero)
            let index = numeroString.index(numeroString.endIndex, offsetBy: indice)
            
            //let index = numeroString.index(numeroString.startIndex, offsetBy: indice)
            let primeraParte = numeroString.prefix(upTo: index)
            let segundaParte = numeroString.suffix(from: index)
            print("numero: \(numeroString). PrimeraParte: \(primeraParte). SegundaParte: \(segundaParte)")
            
            
            let primerValor = Float(primeraParte)
            let segundoValor = Float(segundaParte)
            return (primerValor ?? 0.0, segundoValor ?? 0.0)
        */
    }
    func redondeoCinco(numero: Float) -> Float{
        let numeroString = String(round(numero))
        let index = numeroString.index(numeroString.endIndex, offsetBy: -1)
        var ultimoDigito = numeroString[index]
        switch ultimoDigito {
            case "0", "1", "2", "3", "4":
                ultimoDigito = "0"
            default:
                ultimoDigito = "5"
            }
        let primeraParte = numeroString.prefix(upTo: index)
        let nuevoNumero = primeraParte + String(ultimoDigito)
        let valorRedondeado = Float(nuevoNumero)
        return valorRedondeado!
    }
    func calculoParametos(){
        switch marcadorCampo {
            case "calTotalTF":
                caloriasTotales = caloriasTotales0 * 100 + caloriasTotales1 * 5
                print("caloriasTotales0: \(caloriasTotales0). caloriasTotales1: \(caloriasTotales1)")
                print("calorias totales: \(caloriasTotales)")
                calKg = caloriasTotales / pesoCalculos
                calKg0 = separarEnteroDecimal(numero: calKg).0
                calKg1 = round(separarEnteroDecimal(numero: calKg).1 * 10) / 10
                
                let HB = harrisBeneditt(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
                calHarris = round (caloriasTotales * 100 / HB) / 100
                calHarris0 = (separarHBNumero(numero: calHarris, indice: -1).0)
                calHarris1 = (separarHBNumero(numero: calHarris, indice: -1).1)

                print("Harris: \(calHarris)")
                print("Harris0: \(calHarris0). Harris1: \(calHarris1)")
                

                HdeC = caloriasTotales * HdeCLipidos / (4 * 100)
                HdeC0 = round (separarPartesNumero(numero: HdeC, indice: -3).0)
                HdeC1 = round (separarPartesNumero(numero: HdeC, indice: -3).1)
                lipidos = caloriasTotales * (100 - HdeCLipidos) / (9 * 100)
                lipidos0 = round (separarPartesNumero(numero: lipidos, indice: -3).0)
                lipidos1 = round (separarPartesNumero(numero: lipidos, indice: -3).1)
               /*
                HdeCLipidos = HdeC * 4 * 100 / (lipidos * 9)
                HdeCLipidos0 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).0) * 10
                HdeCLipidos1 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).1)
                */
 
            case "calKgTF":
                calKg = calKg0 + calKg1
                
                caloriasTotales = redondeoCinco(numero: (calKg * pesoCalculos))
                caloriasTotales0 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).0)
                caloriasTotales1 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).1)

                let HB = harrisBeneditt(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
                calHarris = round (caloriasTotales * 100 / HB) / 100
                calHarris0 = (separarHBNumero(numero: calHarris, indice: -1).0)
                calHarris1 = (separarHBNumero(numero: calHarris, indice: -1).1)
                
                HdeC = caloriasTotales * HdeCLipidos / (4 * 100)
                HdeC0 = round (separarPartesNumero(numero: HdeC, indice: -3).0)
                HdeC1 = round (separarPartesNumero(numero: HdeC, indice: -4).1)
                
                lipidos = caloriasTotales * (100 - HdeCLipidos) / (9 * 100)
                lipidos0 = round (separarPartesNumero(numero: lipidos, indice: -3).0)
                lipidos1 = round (separarPartesNumero(numero: lipidos, indice: -4).1)
            
            case "calHarrisTF":
                let HB = harrisBeneditt(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
                harrisBenedict = calHarris0 + calHarris1
                caloriasTotales = redondeoCinco(numero: HB * harrisBenedict)
                print("Calorías redondeo: \(caloriasTotales)")
                caloriasTotales0 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).0)
                caloriasTotales1 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).1)
                
                calKg = caloriasTotales / pesoCalculos
                calKg0 = separarEnteroDecimal(numero: calKg).0
                calKg1 = round(separarEnteroDecimal(numero: calKg).1 * 10) / 10
            
                HdeC = caloriasTotales * HdeCLipidos / (4 * 100)
                HdeC0 = round (separarPartesNumero(numero: HdeC, indice: -3).0)
                HdeC1 = round (separarPartesNumero(numero: HdeC, indice: -3).1)

                lipidos = caloriasTotales * (100 - HdeCLipidos) / (9 * 100)
                lipidos0 = round (separarPartesNumero(numero: lipidos, indice: -3).0)
                lipidos1 = round (separarPartesNumero(numero: lipidos, indice: -3).1)
            
            case "HdeCLipidosTF":
                HdeCLipidos = (HdeCLipidos0 + HdeCLipidos1)
                HdeC = caloriasTotales * HdeCLipidos / (4 * 100)
                HdeC0 = round (separarPartesNumero(numero: HdeC, indice: -3).0)
               // HdeC0 = round ((valorH - 20) / 10)
                HdeC1 = round (separarPartesNumero(numero: HdeC, indice: -3).1)
                
                lipidos = caloriasTotales * (100 - HdeCLipidos) / (9 * 100)
                lipidos0 = round (separarPartesNumero(numero: lipidos, indice: -3).0)
                //lipidos0 = round ((valorL - 20) / 10)
                lipidos1 = round (separarPartesNumero(numero: lipidos, indice: -3).1)
          
            
            case "HdeCTF":
                HdeC = HdeC0 * 10 + HdeC1
                
                caloriasTotales = redondeoCinco(numero: (HdeC * 4 + lipidos * 9))
                caloriasTotales0 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).0)
                caloriasTotales1 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).1)
                
                calKg = caloriasTotales / pesoCalculos
                calKg0 = separarEnteroDecimal(numero: calKg).0
                calKg1 = round(separarEnteroDecimal(numero: calKg).1 * 10) / 10
                
                let HB = harrisBeneditt(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
                calHarris = round (caloriasTotales * 100 / HB) / 100
                calHarris0 = (separarHBNumero(numero: calHarris, indice: -1).0)
                calHarris1 = (separarHBNumero(numero: calHarris, indice: -1).1)
                
                HdeCLipidos = HdeC * 4 * 100 / (caloriasTotales)
                HdeCLipidos0 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).0)
                HdeCLipidos1 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).1)
                print("Harris: \(calHarris)")
                print("Harris0: \(calHarris0). Harris1: \(calHarris1)")
            case "lipidosTF":
                lipidos = lipidos0 * 10 + lipidos1
                
                caloriasTotales = redondeoCinco(numero: (HdeC * 4 + lipidos * 9))
                caloriasTotales0 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).0)
                caloriasTotales1 = round (separarPartesNumero(numero: caloriasTotales, indice: -4).1)
                
                calKg = caloriasTotales / pesoCalculos
                calKg0 = separarEnteroDecimal(numero: calKg).0
                calKg1 = round(separarEnteroDecimal(numero: calKg).1 * 10) / 10
                
                let HB = harrisBeneditt(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
                calHarris = round (caloriasTotales * 100 / HB) / 100
                calHarris0 = (separarHBNumero(numero: calHarris, indice: -1).0)
                calHarris1 = (separarHBNumero(numero: calHarris, indice: -1).1)
                
                HdeCLipidos = HdeC * 4 * 100 / (caloriasTotales)
                HdeCLipidos0 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).0)
                HdeCLipidos1 = round (separarPartesNumero(numero: HdeCLipidos, indice: -2).1)

            default:
                break
            
            }
            rellenarCampos()
 
    }
    func rellenarCampos(){
        calTotalTF.text = String(format: "%.0f", (caloriasTotales0 * 100 + caloriasTotales1))
        calHarrisTF.text = String(format: "%.2f", (calHarris0 + calHarris1))
        calKgTF.text = String(format: "%.1f", (calKg0 + calKg1))
        HdeCLipidosTF.text = String(format: "%.0f", (HdeCLipidos0 + HdeCLipidos1))
        HdeCTF.text = String(format: "%.0f", (HdeC0 * 10 + HdeC1))
        lipidosTF.text = String(format: "%.0f", (lipidos0 * 10 + lipidos1))
           }
    
    
    
    @objc func sliderValueDidChange(_ sender:UISlider!){

        switch marcadorCampo {
            case "calTotalTF":
                caloriasTotales0 = round(slider0.value)
                caloriasTotales1 = round(slider1.value)
            
            case "calKgTF":
                calKg0 = round(slider0.value)
                calKg1 = round(slider1.value * 10) / 10

            case "calHarrisTF":
                calHarris0 = round(slider0.value * 10) / 10
                calHarris1 = round(slider1.value * 100) / 100
                print("Harris0: \(calHarris0). Harris1: \(calHarris1)")
            
            case "HdeCTF":
                HdeC0 = round(slider0.value)
                HdeC1 = round(slider1.value)
            
            case "lipidosTF":
                lipidos0 = round(slider0.value)
                lipidos1 = round(slider1.value)
            
            case "HdeCLipidosTF":
                HdeCLipidos0 = round(slider0.value)
                HdeCLipidos1 = round(slider1.value)

            case "nitrogenoTF":
                nitrogeno0 = round(slider0.value)
                nitrogeno1 = round(slider1.value * 10) / 10
                let valorTotal = nitrogeno0 + nitrogeno1
                nitrogenoTF.text = String(valorTotal)
            
            default:
                break
        }

        calculoParametos()

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case pesoTF:
            marcadorCampo = "pesoTF"
            minimoValorSlider0 = 4
            maximoValorSlider0 = 15
            minimoValorSlider1 = 0
            maximoValorSlider1 = 19
            slider0Posición = caloriasTotales0
            slider1Posición = caloriasTotales1
        case calTotalTF:
            marcadorCampo = "calTotalTF"
            minimoValorSlider0 = 4
            maximoValorSlider0 = 35
            minimoValorSlider1 = 0
            maximoValorSlider1 = 19
            slider0Posición = caloriasTotales0
            slider1Posición = caloriasTotales1
        case calKgTF:
            marcadorCampo = "calKgTF"
            minimoValorSlider0 = 5
            maximoValorSlider0 = 40
            minimoValorSlider1 = 0.0
            maximoValorSlider1 = 0.9
            slider0Posición = calKg0
            slider1Posición = calKg1
        case calHarrisTF:
            marcadorCampo = "calHarrisTF"
            minimoValorSlider0 = 0.1
            maximoValorSlider0 = 3
            minimoValorSlider1 = 0.00
            maximoValorSlider1 = 0.09
            slider0Posición = calHarris0
            slider1Posición = calHarris1
        case HdeCLipidosTF:
            marcadorCampo = "HdeCLipidosTF"
            minimoValorSlider0 = 0
            maximoValorSlider0 = 90
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = HdeCLipidos0
            slider1Posición = HdeCLipidos1
        case HdeCTF:
            marcadorCampo = "HdeCTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = HdeC0
            slider1Posición = HdeC1
        case lipidosTF:
            marcadorCampo = "lipidosTF"
            minimoValorSlider0 = 2
            maximoValorSlider0 = 39
            minimoValorSlider1 = 0
            maximoValorSlider1 = 9
            slider0Posición = lipidos0
            slider1Posición = lipidos1
            
            
        case nitrogenoTF:
            marcadorCampo = "nitrogenoTF"
            minimoValorSlider0 = 5
            maximoValorSlider0 = 30
            minimoValorSlider1 = 0
            maximoValorSlider1 = 0.9
            slider0Posición = nitrogeno0
            slider1Posición = nitrogeno1
            
        default:
            break
        }
        slider()
    }
}


