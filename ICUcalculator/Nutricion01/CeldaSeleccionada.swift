//
//  CeldaSeleccionada.swift
//  Nutricion01
//
//  Created by Julio Barado Hualde on 21/02/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class CeldaSeleccionada: UITableViewCell {
    
    
    @IBOutlet var etiquetas: UILabel!
    @IBOutlet var valores: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        etiquetas.textColor = .red
        valores.textColor = .red
        
    }
    
}
